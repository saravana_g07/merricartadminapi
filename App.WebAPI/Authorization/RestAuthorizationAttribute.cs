﻿using App.Services.Helper;
using App.Services.Helper.Classes;
using App.Services.Services;
using App.WebAPI.Controllers.Base;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;

namespace App.WebAPI.Authorization
{
    public class RestAuthorizationAttribute : AuthorizeAttribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            try
            {
                AppSetup appSetup = BaseHeper.AppInstance.GetAppSetup();
                //if (context.HttpContext.Request.Host.Value == appSetup.AppConfig.ClientAppUrl)
                //{
                string authorizationtoken = context.HttpContext.Request.Headers["authorizationtoken"].ToString();
                Session session = AuthenticationServices.Instance.IsValidSession(authorizationtoken);
                if (session == null)
                {
                    context.Result = new UnauthorizedResult();
                }
                else
                {
                    if (BaseApiController.sessionManager == null)
                    {
                        BaseApiController.sessionManager = new SessionManager();
                    }
                    BaseApiController.sessionManager.Session = session;
                }
                //}
                //else
                //{
                //    context.HttpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                //}
            }
            catch (Exception e)
            {
                context.Result = new UnauthorizedResult();
            }

        }
    }
}