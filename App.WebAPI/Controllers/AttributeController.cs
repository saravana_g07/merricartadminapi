﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using App.DAL.FilterModel;
using App.DAL.Model;
using App.Services.DTO.Model;
using App.Services.Services;
using App.Services.Services.IServices;
using App.WebAPI.Authorization;
using App.WebAPI.Controllers.Base;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using ServiceStack.Web;

namespace App.WebAPI.Controllers
{
    [ApiController]
    [RestAuthorization]
    [EnableCors("AllowOrigin")]
    [Route("api/[controller]")]
    public class AttributeController : BaseApiController
    {
        private IAttributeServices _attributeServices;



        public AttributeController(IConfiguration iconfiguration) : base(iconfiguration)
        {
            _attributeServices = AttributeServices.Instance;
        }

        [Route("[action]")]
        [HttpGet]
        public DataTableResponse GetAllAttributes()
        {
            try
            {
                DataTableFilter filter = new DataTableFilter();
                filter.SortingColumnIndex = Convert.ToInt32(HttpContext.Request.Query["order[0][column]"].ToString());
                filter.OrderBy = HttpContext.Request.Query["order[0][dir]"].ToString();
                filter.SearchText = HttpContext.Request.Query["search[value]"].ToString();
                filter.Start = Convert.ToInt32(HttpContext.Request.Query["start"].ToString());
                filter.Length = Convert.ToInt32(HttpContext.Request.Query["length"].ToString());

                Tuple<List<Attributes>, int> dataResult = _attributeServices.GetAllAttributes(sessionManager, filter);

                return new DataTableResponse(dataResult.Item2, dataResult.Item1.Count, dataResult.Item1);
            }
            catch (Exception)
            {
                //log the exception here
                return null;
            }
            finally
            {
            }
        }


        [Route("[action]")]
        [HttpGet]
        public IHttpResult GetAllActiveAttributes()
        {
            try
            {
                result.Response = _attributeServices.GetAllActiveAttributes(sessionManager);
            }
            catch (Exception)
            {
                //log the exception here
            }
            finally
            {
            }
            return result;
        }


        [Route("[action]")]
        [HttpPost]
        public IHttpResult InsertAttribute([FromBody]Attributes attribute)
        {
            try
            {

                result.Response = _attributeServices.InsertAttribute(sessionManager, attribute);
            }
            catch (Exception)
            {
                //log the exception here
                result.Response = null;
            }
            finally
            {
            }
            return result;
        }

        [Route("[action]")]
        [HttpPost]
        public IHttpResult UpdateAttribute([FromBody]Attributes attribute)
        {
            try
            {
                result.Response = _attributeServices.UpdateAttribute(sessionManager, attribute);
            }
            catch (Exception)
            {
                //log the exception here
                result.Response = null;
            }
            finally
            {
            }
            return result;
        }

        [Route("[action]/{attributeId}")]
        [HttpGet]
        public IHttpResult DeleteAttribute(int attributeId)
        {
            try
            {
                result.Response = _attributeServices.DeleteAttribute(sessionManager, attributeId);
            }
            catch (Exception)
            {
                //log the exception here
                result.Response = null;
            }
            finally
            {
            }
            return result;
        }


    }
}