﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using App.DAL.Model;
using App.Services.Services;
using App.Services.Services.IServices;
using App.WebAPI.Authorization;
using App.WebAPI.Controllers.Base;
using Microsoft.AspNetCore.Cors;
//using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;
using ServiceStack.Web;

namespace App.WebAPI.Controllers
{
    [ApiController]
    [RestAuthorization]
    [EnableCors("AllowOrigin")]
    [Route("api/[controller]")]
    public class CategoryController : BaseApiController
    {
        private ICategoryServices _categoryServices;
        

        public CategoryController(IConfiguration iconfiguration) :base(iconfiguration)
        {
            _categoryServices = CategoryServices.Instance;
        }

        [Route("[action]")]
        [HttpGet]
        public IHttpResult GetAllCategoriesInHierarchy()
        {
            try
            {
                result.Response = _categoryServices.GetAllCategoriesInHierarchy(sessionManager);
            }
            catch (Exception)
            {
                //log the exception here
                result.Response = null;
            }
            finally
            {
            }
            return result;
        }

        [Route("[action]")]
        [HttpPost]
        public IHttpResult CreateNewCategory([FromBody]Categories categoryInfo)
        {
            try
            {
                result.Response = _categoryServices.CreateNewCategory(sessionManager, categoryInfo);
            }
            catch (Exception)
            {
                //log the exception here
                result.Response = null;
            }
            finally
            {
            }
            return result;
        }

        [Route("[action]/{categoryId}")]
        [HttpGet]
        public IHttpResult DeleteCategory(int categoryId)
        {
            try
            {
                result.Response = _categoryServices.DeleteCategogy(sessionManager, categoryId);
            }
            catch (Exception)
            {
                //log the exception here
                result.Response = false;
            }
            finally
            {
            }
            return result;
        }

        [Route("[action]")]
        [HttpPost]
        public IHttpResult UpdateCategory([FromBody]Categories categoryInfo)
        {
            try
            {
                //Categories categoryInfo = new Categories();
                result.Response = _categoryServices.UpdateCategory(sessionManager, categoryInfo);
            }
            catch (Exception)
            {
                //log the exception here
                result.Response = null;
            }
            finally
            {
            }
            return result;
        }

        [Route("[action]")]
        [HttpPost]
        public IHttpResult UpdateActiveCategory([FromBody]List<int> nonActiveCatehoryIds)
        {
            try
            {
                result.Response = _categoryServices.SetActiveCategory(sessionManager, nonActiveCatehoryIds);
            }
            catch (Exception)
            {
                //log the exception here
                result.Response = null;
            }
            finally
            {
            }
            return result;
        }
    }
}