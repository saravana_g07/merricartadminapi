﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using App.DAL.FilterModel;
using App.DAL.Model;
using App.Services.DTO.Model;
using App.Services.Services;
using App.Services.Services.IServices;
using App.WebAPI.Authorization;
using App.WebAPI.Controllers.Base;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using ServiceStack.Web;

namespace App.WebAPI.Controllers
{
    [ApiController]
    [RestAuthorization]
    [EnableCors("AllowOrigin")]
    [Route("api/[controller]")]
    public class UserController : BaseApiController
    {
        private IUserServices _userServices;

        public UserController(IConfiguration iconfiguration) : base(iconfiguration)
        {
            _userServices = UserServices.Instance;
        }

        [Route("[action]")]
        [HttpGet]
        public DataTableResponse GetAllUsers()
        {
            try
            {
                DataTableResponse response = new DataTableResponse();
                DataTableFilter filter = new DataTableFilter();
                filter.SortingColumnIndex = Convert.ToInt32(HttpContext.Request.Query["order[0][column]"].ToString());
                filter.OrderBy = HttpContext.Request.Query["order[0][dir]"].ToString();
                filter.SearchText = HttpContext.Request.Query["search[value]"].ToString();
                filter.Start = Convert.ToInt32(HttpContext.Request.Query["start"].ToString());
                filter.Length = Convert.ToInt32(HttpContext.Request.Query["length"].ToString());

                Tuple<List<IUserInfo>, int> dataResult = _userServices.GetAllUsers(sessionManager, filter);

                response = new DataTableResponse(dataResult.Item2, dataResult.Item1.Count, dataResult.Item1);
                return response;
            }
            catch (Exception)
            {
                //log the exception here
                result.Response = null;
            }
            finally
            {
            }
            return null;
        }

        [Route("[action]")]
        [HttpPost]
        public IHttpResult InsertUser([FromBody]User user)
        {
            try
            {

                result.Response = _userServices.InsertUser(sessionManager, user);
            }
            catch (Exception)
            {
                //log the exception here
                result.Response = null;
            }
            finally
            {
            }
            return result;
        }

        [Route("[action]")]
        [HttpPost]
        public IHttpResult UpdateUser([FromBody]User user)
        {
            try
            {
                result.Response = _userServices.UpdateUser(sessionManager, user);
            }
            catch (Exception)
            {
                //log the exception here
                result.Response = null;
            }
            finally
            {
            }
            return result;
        }
        [Route("[action]")]
        [HttpPost]
        public IHttpResult UpdateMyProfile([FromBody]User user)
        {
            try
            {
                result.Response = _userServices.UpdateMyProfile(sessionManager, user);
            }
            catch (Exception)
            {
                //log the exception here
                result.Response = null;
            }
            finally
            {
            }
            return result;
        }

        [Route("[action]/{userId}")]
        [HttpGet]
        public IHttpResult DeleteUser(int userId)
        {
            try
            {
                result.Response = _userServices.DeleteUser(sessionManager, userId);
            }
            catch (Exception)
            {
                //log the exception here
                result.Response = null;
            }
            finally
            {
            }
            return result;
        }

    }
}