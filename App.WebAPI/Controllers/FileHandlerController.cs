﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using App.Services.Services;
using App.WebAPI.Authorization;
using App.WebAPI.Controllers.Base;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using ServiceStack.Web;

namespace App.WebAPI.Controllers
{
    [ApiController]
    [RestAuthorization]
    [EnableCors("AllowOrigin")]
    [Route("api/[controller]")]
    public class FileHandlerController : BaseApiController
    {


        public FileHandlerController(IConfiguration iconfiguration) : base(iconfiguration)
        {
        }

        [Route("[action]")]
        [HttpPost]
        public IHttpResult UploadDataUrlAsImage([FromBody]dynamic uploadObj)
        {
            try
            {
                string dataUrl = uploadObj.GetProperty("dataUrl").ToString();
                string uploadImageType = uploadObj.GetProperty("uploadImageType").ToString();
                if (uploadImageType == "user")
                {
                    result.Response = UserServices.Instance.UpdateUserImage(sessionManager, dataUrl).ToString();
                    result.StatusCode = System.Net.HttpStatusCode.Created;
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                //log the exception here
                result.Response = null;
                result.StatusCode = System.Net.HttpStatusCode.BadRequest;
                result.StatusDescription = ex.Message;
            }
            finally
            {
            }
            return result;
        }
    }
}