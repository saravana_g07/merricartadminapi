﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using App.DAL.Model;
using App.Services.Services;
using App.Services.Services.IServices;
using App.WebAPI.Controllers.Base;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using ServiceStack.Web;

namespace App.WebAPI.Controllers
{
    [ApiController]
    [EnableCors("AllowOrigin")]
    [Route("api/[controller]")]
    public class AuthenticationController : BaseApiController
    {
        private IAuthenticationServices _authenticationServices;


        public AuthenticationController(IConfiguration iconfiguration) : base(iconfiguration)
        {
            _authenticationServices = AuthenticationServices.Instance;
        }

        [Route("[action]")]
        [HttpPost]
        public IHttpResult SigninUser([FromBody]dynamic credentials)
        {
            try
            {
                string loginEmail = credentials.GetProperty("loginEmail").ToString();
                string password = credentials.GetProperty("password").ToString();
                result.Response = _authenticationServices.LogInUser(sessionManager, loginEmail, password);
                result.StatusCode = System.Net.HttpStatusCode.Created;
            }
            catch (MemberAccessException mEx)
            {
                result.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                result.StatusDescription = mEx.Message;
            }
            catch (Exception)
            {
                result.StatusCode = System.Net.HttpStatusCode.BadRequest;
                result.StatusDescription = "Something went wrong! Please contact administartor.";
            }
            finally
            {
            }
            return result;
        }

        [Route("[action]")]
        [HttpGet]
        public IHttpResult SignOutUser()
        {
            try
            {
                string accessToken = "";
                result.Response = _authenticationServices.LogoutUser(accessToken);
            }
            catch (Exception)
            {
                result.StatusCode = System.Net.HttpStatusCode.BadRequest;
                result.StatusDescription = "Something went wrong! Please contact administartor.";
            }
            finally
            {
            }
            return result;
        }

        [Route("[action]")]
        [HttpPost]
        public IHttpResult ForgotPassword([FromBody]dynamic email)
        {
            try
            {
                string loginEmail = email.GetProperty("loginEmail").ToString();
                result.Response = _authenticationServices.ForgotPassword(sessionManager, loginEmail);
                result.StatusCode = System.Net.HttpStatusCode.Created;
                result.StatusDescription = "If you are member, You will be received Temporaray password to your mailbox, ";
            }
            catch (Exception)
            {
                result.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                result.StatusDescription = "Something went wrong. Please contact administrator!";
            }
            finally
            {
            }
            return result;
        }

        [Route("[action]")]
        [HttpPost]
        public IHttpResult ResetPassword([FromBody]ResetPassword password)
        {
            try
            {
                result.Response = _authenticationServices.ResetPassword(sessionManager, password);
                result.StatusCode = System.Net.HttpStatusCode.Created;
            }
            catch (MemberAccessException mEx)
            {
                result.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                result.StatusDescription = mEx.Message;
            }
            catch (Exception)
            {
                result.StatusCode = System.Net.HttpStatusCode.BadRequest;
                result.StatusDescription = "Something went wrong! Please contact administartor.";
            }
            finally
            {
            }
            return result;
        }

    }
}