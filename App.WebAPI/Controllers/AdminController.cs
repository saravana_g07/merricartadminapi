﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using App.DAL.FilterModel;
using App.DAL.Model;
using App.Services.DTO.Model;
using App.Services.Services;
using App.Services.Services.IServices;
using App.WebAPI.Authorization;
using App.WebAPI.Controllers.Base;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using ServiceStack.Web;

namespace App.WebAPI.Controllers
{
    [ApiController]
    [RestAuthorization]
    [EnableCors("AllowOrigin")]
    [Route("api/[controller]")]
    public class AdminController : BaseApiController
    {
        private IAdminServices _adminServices;

        public AdminController(IConfiguration iconfiguration) : base(iconfiguration)
        {
            _adminServices = AdminServices.Instance;
        }

        [Route("[action]")]
        [HttpGet]
        public IHttpResult GetAllRoles()
        {
            try
            {
                result.Response = _adminServices.GetAllRoles(sessionManager);
            }
            catch (Exception)
            {
                //log the exception here
            }
            return result;
        }

        [Route("[action]")]
        [HttpGet]
        public IHttpResult GetAllModules()
        {
            try
            {
                result.Response = _adminServices.GetAllModules(sessionManager);
            }
            catch (Exception)
            {
                //log the exception here
            }
            return result;
        }

        [Route("[action]")]
        [HttpGet]
        public IHttpResult GetAllModulesHierarchy()
        {
            try
            {
                result.Response = _adminServices.GetAllModulesHierarchy(sessionManager);
            }
            catch (Exception)
            {
                //log the exception here
                result.Response = null;
            }
            finally
            {
            }
            return result;
        }

        [Route("[action]")]
        [HttpGet]
        public DataTableResponse GetRoleList()
        {
            try
            {
                DataTableResponse response = new DataTableResponse();
                DataTableFilter filter = new DataTableFilter();
                filter.SortingColumnIndex = Convert.ToInt32(HttpContext.Request.Query["order[0][column]"].ToString());
                filter.OrderBy = HttpContext.Request.Query["order[0][dir]"].ToString();
                filter.SearchText = HttpContext.Request.Query["search[value]"].ToString();
                filter.Start = Convert.ToInt32(HttpContext.Request.Query["start"].ToString());
                filter.Length = Convert.ToInt32(HttpContext.Request.Query["length"].ToString());

                Tuple<List<RoleInfo>, int> dataResult = _adminServices.GetRoleList(sessionManager, filter);

                response = new DataTableResponse(dataResult.Item2, dataResult.Item1.Count, dataResult.Item1);
                return response;
            }
            catch (Exception)
            {
                //log the exception here
                result.Response = null;
            }
            finally
            {
            }
            return null;
        }
        
        [Route("[action]")]
        [HttpPost]
        public IHttpResult createRole([FromBody]SystemRole role)
        {
            try
            {
                result.Response = _adminServices.CreateRole(sessionManager, role);
            }
            catch (Exception)
            {
                //log the exception here
                result.Response = null;
            }
            finally
            {
            }
            return result;
        }

        [Route("[action]/{roleId}")]
        [HttpGet]
        public IHttpResult DeleteRole(int roleId)
        {
            try
            {
                result.Response = _adminServices.DeleteRole(sessionManager, roleId);
            }
            catch (Exception)
            {
                //log the exception here
                result.Response = false;
            }
            finally
            {
            }
            return result;
        }

        [Route("[action]")]
        [HttpPost]
        public IHttpResult UpdateRole([FromBody]SystemRole role)
        {
            try
            {
                result.Response = _adminServices.UpdateRole(sessionManager, role);
            }
            catch (Exception)
            {
                //log the exception here
                result.Response = null;
            }
            finally
            {
            }
            return result;
        }


    }
}