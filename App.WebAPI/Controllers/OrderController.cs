﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using App.WebAPI.Authorization;
using App.WebAPI.Controllers.Base;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace App.WebAPI.Controllers
{
    [ApiController]
    [RestAuthorization]
    [EnableCors("AllowOrigin")]
    [Route("api/[controller]")]
    public class OrderController : BaseApiController
    {
        

        public OrderController(IConfiguration iconfiguration) :base(iconfiguration)
        {
        }
    }
}