﻿using System;
using System.Collections.Generic;
using App.DAL.FilterModel;
using App.DAL.Model;
using App.Services.DTO.Model;
using App.Services.Services;
using App.Services.Services.IServices;
using App.WebAPI.Controllers.Base;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using ServiceStack.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using App.WebAPI.Authorization;

namespace App.WebAPI.Controllers
{
    [ApiController]
    [RestAuthorization]
    [EnableCors("AllowOrigin")]
    [Route("api/[controller]")]
    public class ProductController : BaseApiController
    {
        private IProductServices _productServices;


        public ProductController(IConfiguration iconfiguration) : base(iconfiguration)
        {
            _productServices = ProductServices.Instance;
        }


        [Route("[action]")]
        [HttpPost]
        public DataTableResponse GetAllProducts()
        {
            try
            {
                DataTableResponse response = new DataTableResponse();
                DataTableFilter filter = new DataTableFilter();
                filter.SortingColumnIndex = Convert.ToInt32(HttpContext.Request.Form["order[0][column]"].ToString());
                filter.OrderBy = HttpContext.Request.Form["order[0][dir]"].ToString();
                filter.SearchText = HttpContext.Request.Form["search[value]"].ToString();
                filter.Start = Convert.ToInt32(HttpContext.Request.Form["start"].ToString());
                filter.Length = Convert.ToInt32(HttpContext.Request.Form["length"].ToString());

                Tuple<List<IProductList>, int> dataResult = _productServices.GetAllProducts(sessionManager, filter);

                response = new DataTableResponse(dataResult.Item2, dataResult.Item1.Count, dataResult.Item1);
                return response;

            }
            catch (Exception)
            {
                //log the exception here
                result.Response = null;
            }
            finally
            {
            }
            return null;
        }

        [Route("[action]")]
        [HttpGet]
        public IHttpResult GetProductByID()
        {
            try
            {
                string id = HttpContext.Request.Query["id"].ToString();
                result.Response = _productServices.GetProductByID(sessionManager, id);
            }
            catch (Exception)
            {
                //log the exception here
                result.Response = null;
            }
            finally
            {
            }
            return result;
        }


        [Route("[action]")]
        [HttpGet]
        public IHttpResult GetAllCategories()
        {
            try
            {
                result.Response = _productServices.GetAllCategories(sessionManager);
            }
            catch (Exception)
            {
                result.Response = null;
            }
            finally
            {
            }
            return result;
        }

        [Route("[action]")]
        [HttpPost]
        public IHttpResult CreateNewProduct([FromBody]dynamic productInfo)
        {
            try
            {
                Products product = JsonConvert.DeserializeObject<Products>(productInfo.GetProperty("productInfo").GetRawText());
                List<string> newProductImageDataUrl = JsonConvert.DeserializeObject<List<string>>(productInfo.GetProperty("newProductImageDataUrl").GetRawText());
                result.Response = _productServices.CreateNewProduct(sessionManager, product, newProductImageDataUrl);
            }
            catch (Exception ex)
            {
                //log the exception here
                result.Response = null;
            }
            finally
            {
            }
            return result;
        }

        [Route("[action]/{productId}")]
        [HttpGet]
        public IHttpResult DeleteProduct(int productId)
        {
            try
            {
                result.Response = _productServices.DeleteProduct(sessionManager, productId);
            }
            catch (Exception)
            {
                //log the exception here
                result.Response = false;
            }
            finally
            {
            }
            return result;
        }

        [Route("[action]")]
        [HttpPost]
        public IHttpResult UpdateProduct([FromBody]dynamic productInfo)
        {
            try
            {
                Products product = JsonConvert.DeserializeObject<Products>(productInfo.GetProperty("productInfo").GetRawText());
                List<string> productImageToDelete = JsonConvert.DeserializeObject<List<string>>(productInfo.GetProperty("productImageToDelete").GetRawText());
                List<string> newProductImageDataUrl = JsonConvert.DeserializeObject<List<string>>(productInfo.GetProperty("newProductImageDataUrl").GetRawText());

                result.Response = _productServices.UpdateProduct(sessionManager, product, productImageToDelete, newProductImageDataUrl);
            }
            catch (Exception)
            {
                //log the exception here
                result.Response = null;
            }
            finally
            {
            }
            return result;
        }

        [Route("[action]/{productId}")]
        [HttpGet]
        public IHttpResult GetProductImages(int productId)
        {
            try
            {
                result.Response = _productServices.GetProductImage(sessionManager, productId);
            }
            catch (Exception)
            {
                //log the exception here
                result.Response = false;
            }
            finally
            {
            }
            return result;
        }

    }
}
