﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using App.DAL;
using App.Services.Helper.Classes;
using App.Services.Services;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
//using ServiceStack;
using ServiceStack.Web;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace App.WebAPI.Controllers.Base
{
    public class BaseApiController : ControllerBase
    {
        public static IHttpResult result;
        public static SessionManager sessionManager;
        public BaseApiController(IConfiguration iconfiguration)
        {
            try
            {
                result = new ServiceStack.HttpResult();
                if (sessionManager == null)
                {
                    sessionManager = new SessionManager();
                }
                sessionManager.UnitOfWork = new UnitOfWork(iconfiguration);
            }
            catch
            {//do nothing 
            }

        }
    }
}
