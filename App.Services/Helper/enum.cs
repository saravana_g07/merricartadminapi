﻿/// <summary>
/// Mail template property should be same as mailtemplate file name
/// </summary>
public enum MailTemplates
{
    NewUserTemplate = 0,
    ForgorPasswordTemplate = 1
}