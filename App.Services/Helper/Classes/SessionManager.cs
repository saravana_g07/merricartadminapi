﻿using App.DAL;
using App.DAL.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace App.Services.Helper.Classes
{
    public class SessionManager
    {
        public Session Session { get; set; }
        public IUnitOfWork UnitOfWork { get; set; }
    }
}
