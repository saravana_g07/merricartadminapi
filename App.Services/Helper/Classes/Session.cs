﻿using Amazon.CognitoIdentityProvider.Model;
using App.DAL.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace App.Services.Helper.Classes
{
    public class Session
    {
        public string SessionToken { get; set; }
        public UserInfo User { get; set; }
        public int IdleLogoutTime { get; set; }
        public List<ModuleHierarchy> ModuleHierarchy { get; set; }
    }
}
