﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App.Services.Helper.Classes
{
    public class Email
    {
        public string EmailReceiverId { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
    }

    public class EmailBody
    {
        public MailTemplates MailTemplateIdentifier { get; set; }
        public string UserName { get; set; }
        public string TempPassword { get; set; }
    }
}
