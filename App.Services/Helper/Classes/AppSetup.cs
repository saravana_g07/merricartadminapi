﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App.Services.Helper.Classes
{
    public class AppSetup
    {
        public string AppBasePath { get; set; }
        public AppConfig AppConfig { get; set; }
        public MailConfig MailConfig { get; set; }
        public UserConfig UserConfig { get; set; }
        public RedisConfig RedisConfig { get; set; }
        public AWSConfig AWSConfig { get; set; }
    }

    #region Nested classes

    public class AppConfig
    {
        public string ClientAppUrl { get; set; }
        public string TempUploadFolder { get; set; }
        public string MailTempatesFolder { get; set; }

    }
    public class MailConfig
    {
        public string SmtpClient { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public int Port { get; set; }
        public bool EnableSsl { get; set; }
    }
    public class UserConfig
    {
        public string DefaultPassword { get; set; }
        public int IdleLogoutTimeInMinutes { get; set; }
    }
    public class RedisConfig
    {
        public string RedisServer { get; set; }
        public int RedisDatabaseNumber { get; set; }
    }
    public class AWSConfig
    {
        public string Region { get; set; }
        public string AccessKeyId { get; set; }
        public string SecretAccessKey { get; set; }
        public CognitoConfig CognitoConfig { get; set; }
        public S3Config S3Config { get; set; }
    }
    public class CognitoConfig
    {
        public string AppClientId { get; set; }
        public string PoolId { get; set; }
    }
    public class S3Config
    {
        public string BucketName { get; set; }
        public string ServiceURL { get; set; }
        public string RegionEndPoint { get; set; }
        public string UploaderURL { get; set; }
    }

    #endregion Nested classes

}
