﻿using App.Services.Helper.Classes;
using Newtonsoft.Json;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Text;

namespace App.Services.Helper
{
    internal static class AppCacheHelper<T>
    {
        #region Initiailize AppCache

        private static IDatabase appCache = null;

        static AppCacheHelper()
        {
            AppSetup appSetup = BaseHeper.AppInstance.GetAppSetup();
            ConnectionMultiplexer redis = ConnectionMultiplexer.Connect(appSetup.RedisConfig.RedisServer);
            appCache = redis.GetDatabase(appSetup.RedisConfig.RedisDatabaseNumber);
        }

        #endregion

        #region Communicate with cache server

        /// <summary>
        /// It will retrive the value for the given key stored in the cache cluster
        /// </summary>
        /// <param name="cacheKey"></param>
        /// <returns></returns>
        private static string Get(string cacheKey)
        {
            try
            {
                string CacheString = appCache.StringGet(cacheKey);
                return CacheString;
            }
            catch
            {
                return default(string);
            }
        }

        /// <summary>
        /// Store (CacheKey,Value) pair to Redis server
        /// </summary>
        /// <param name="cacheObj"> </param>
        /// <param name="cacheKey"> </param>
        private static void Set(string cacheObj, string cacheKey)
        {
            try
            {
                Delete(cacheKey);
                appCache.StringSet(cacheKey, cacheObj); //Redis Cache
            }
            catch
            {
            }
        }

        /// <summary>
        /// Delete cache from Redis server
        /// </summary>
        /// <param name="cacheKey"></param>
        private static void Delete(string cacheKey)
        {
            appCache.KeyDelete(cacheKey);
        }

        #endregion

        #region Public Helper methods

        public static void SetCache(T cacheObj, string cacheKey)
        {
            try
            {
                appCache.KeyDelete(cacheKey);
                Set(JsonConvert.SerializeObject(cacheObj), cacheKey); //Redis Cache
            }
            catch
            {
            }
        }

        public static T GetCache(string cacheKey)
        {
            try
            {
                var value = Get(cacheKey);

                return JsonConvert.DeserializeObject<T>(value);
            }
            catch
            {
                return default(T);
            }
        }

        public static bool DeleteCache(string cacheKey)
        {
            try
            {
                Delete(cacheKey);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        #endregion
    }
}
