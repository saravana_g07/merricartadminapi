﻿using App.Services.Helper.Classes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace App.Services.Helper
{
    public class BaseHeper
    {
        private static string AppBasePath = System.AppDomain.CurrentDomain.BaseDirectory.ToString().Split("bin")[0];

        private static string appSetupPath = AppBasePath + "AppConfig\\appsetup.json";
        private static string appSetupJson = null;
        private static AppSetup appSetup = null;
        private static BaseHeper _appInstance = null;
        private static object syncRoot = new object();


        protected BaseHeper() { }

        public static BaseHeper AppInstance
        {
            get
            {
                if (_appInstance == null)
                {
                    lock (syncRoot)
                    {
                        _appInstance = new BaseHeper();
                    }
                }
                return _appInstance;
            }
        }

        public string GetAppSetupJson()
        {
            if (appSetupJson == null)
            {
                appSetupJson = File.ReadAllText(appSetupPath);
            }
            return appSetupJson;
        }

        public AppSetup GetAppSetup()
        {
            if (appSetup == null)
            {
                appSetup = JsonConvert.DeserializeObject<AppSetup>(GetAppSetupJson());
                appSetup.AppBasePath = AppBasePath;
            }
            return appSetup;
        }
    }
}
