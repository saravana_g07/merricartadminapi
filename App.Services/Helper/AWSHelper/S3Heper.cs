﻿using Amazon.S3;
using Amazon.S3.Model;
using App.Services.Helper.Classes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace App.Services.Helper.AWSHelper
{
    internal static class S3Helper
    {
        #region Initialization

        private static AppSetup appSetup = null;
        public static AmazonS3Client S3Client = null;


        static S3Helper()
        {
            if (appSetup == null)
            {
                appSetup = BaseHeper.AppInstance.GetAppSetup();
            }

            if (S3Client == null)
            {
                S3Client = GetAmazonS3Client();
            }
        }

        private static AmazonS3Client GetAmazonS3Client()
        {
            AmazonS3Config s3Config = new AmazonS3Config()
            {
                ServiceURL = appSetup.AWSConfig.S3Config.ServiceURL,
                RegionEndpoint = Amazon.RegionEndpoint.GetBySystemName(appSetup.AWSConfig.S3Config.RegionEndPoint)
            };
            return new AmazonS3Client(appSetup.AWSConfig.AccessKeyId, appSetup.AWSConfig.SecretAccessKey, s3Config);

        }

        #endregion

        #region Private methonds
        private static PutObjectRequest GeneratePutObjectRequest(string desireKey, string filePath)
        {
            return new PutObjectRequest()
            {
                BucketName = appSetup.AWSConfig.S3Config.BucketName,
                Key = desireKey.Replace("\\", "/"),
                FilePath = filePath,
                CannedACL = S3CannedACL.PublicRead
            };
        }

        private static string CreateTempFile(byte[] content, string fileName)
        {
            try
            {
                string tempFileDirectory = appSetup.AppBasePath + appSetup.AppConfig.TempUploadFolder;
                if (!Directory.Exists(tempFileDirectory))
                {
                    Directory.CreateDirectory(tempFileDirectory);
                }
                string tempFileName = tempFileDirectory + "/" + fileName;

                if (File.Exists(tempFileName))
                {
                    File.Delete(tempFileName);
                }
                File.WriteAllBytes(tempFileName, content);
                return tempFileName;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private static bool DeleteFile(string fileName)
        {
            try
            {
                if (File.Exists(fileName))
                {
                    File.Delete(fileName);
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        #endregion

        #region Helper methods


        public static bool UploadDataUrl(string desireKey, string dataUrl)
        {
            string fileName = "";

            try
            {
                var base64Data = System.Text.RegularExpressions.Regex.Match(dataUrl, @"data:image/(?<type>.+?),(?<data>.+)").Groups["data"].Value;
                byte[] bytes = Convert.FromBase64String(base64Data);

                fileName = CreateTempFile(bytes, desireKey.Split('/')[desireKey.Split('/').Length - 1]);
                if (fileName != null)
                {
                    PutObjectRequest request = GeneratePutObjectRequest(desireKey, fileName);
                    PutObjectResponse response = Task.Run(async () => await S3Client.PutObjectAsync(request)).Result;
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                DeleteFile(fileName);
            }
        }
        public static bool Uploadfile(string desireKey, string filePath)
        {
            try
            {
                using (S3Client)
                {
                    PutObjectRequest request = GeneratePutObjectRequest(desireKey, filePath);
                    PutObjectResponse response = Task.Run(async () => await S3Client.PutObjectAsync(request)).Result;
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static async void UploadfileAsync(string desireKey, string filePath)
        {
            try
            {
                using (S3Client)
                {
                    PutObjectRequest request = GeneratePutObjectRequest(desireKey, filePath);
                    await S3Client.PutObjectAsync(request);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        #endregion
    }
}
