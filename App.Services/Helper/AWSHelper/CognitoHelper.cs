﻿using Amazon;
using Amazon.CognitoIdentityProvider;
using Amazon.CognitoIdentityProvider.Model;
using App.DAL.FilterModel;
using App.DAL.Model;
using App.Services.Helper.Classes;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace App.Services.Helper.AWSHelper
{
    internal class CognitoHelper : BaseHeper
    {
        private static CognitoHelper _instance = null;
        private static AmazonCognitoIdentityProviderClient _cognitoProvider = null;
        private static AppSetup appSetup = null;
        internal static CognitoHelper Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new CognitoHelper();
                }
                return _instance;
            }
        }
        private CognitoHelper() { }

        private AmazonCognitoIdentityProviderClient GetCognitoProvider()
        {
            if (appSetup == null)
            {
                appSetup = AppInstance.GetAppSetup();
            }

            if (_cognitoProvider == null)
            {

                RegionEndpoint regionEndpoint = RegionEndpoint.GetBySystemName(appSetup.AWSConfig.Region);

                //_cognitoProvider = new AmazonCognitoIdentityProviderClient(new Amazon.Runtime.AnonymousAWSCredentials(), regionEndpoint);
                _cognitoProvider = new AmazonCognitoIdentityProviderClient(appSetup.AWSConfig.AccessKeyId, appSetup.AWSConfig.SecretAccessKey, regionEndpoint);

            }
            return _cognitoProvider;
        }

        public SignUpResponse SignUpUser(User user)
        {
            try
            {
                AmazonCognitoIdentityProviderClient cognitoProvider = GetCognitoProvider();
                AppSetup appSetup = AppInstance.GetAppSetup();

                SignUpRequest signUpRequest = new SignUpRequest()
                {
                    ClientId = appSetup.AWSConfig.CognitoConfig.AppClientId,
                    Username = user.LoginEmail,
                    Password = appSetup.UserConfig.DefaultPassword
                };

                List<AttributeType> attributes = new List<AttributeType>()
                {
                    new AttributeType(){Name="email",Value=user.LoginEmail},
                    new AttributeType(){Name="name",Value=user.FirstName},
                    new AttributeType(){Name="family_name",Value=user.LastName},
                    new AttributeType(){Name="custom:UserRole",Value=user.UserRole.ToString()}
                };

                signUpRequest.UserAttributes = attributes;

                return Task.Run(async () => await cognitoProvider.SignUpAsync(signUpRequest)).Result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public InitiateAuthResponse SignInUser(string loginEmail, string password)
        {
            AmazonCognitoIdentityProviderClient cognitoProvider = GetCognitoProvider();
            AppSetup appSetup = AppInstance.GetAppSetup();

            InitiateAuthRequest authRequest = new InitiateAuthRequest
            {
                ClientId = appSetup.AWSConfig.CognitoConfig.AppClientId,
                AuthFlow = AuthFlowType.USER_PASSWORD_AUTH
            };

            authRequest.AuthParameters.Add("USERNAME", loginEmail);
            authRequest.AuthParameters.Add("PASSWORD", password);

            return Task.Run(async () => await cognitoProvider.InitiateAuthAsync(authRequest)).Result;
        }

        public GlobalSignOutResponse SignOutUser(string accessToken)
        {
            AmazonCognitoIdentityProviderClient cognitoProvider = GetCognitoProvider();
            AppSetup appSetup = AppInstance.GetAppSetup();

            GlobalSignOutRequest authRequest = new GlobalSignOutRequest
            {
                AccessToken = accessToken
            };

            return Task.Run(async () => await cognitoProvider.GlobalSignOutAsync(authRequest)).Result;
        }

        public ListUsersResponse GetListOfUsers(DataTableFilter filter = null)
        {
            try
            {
                AmazonCognitoIdentityProviderClient cognitoProvider = GetCognitoProvider();
                AppSetup appSetup = AppInstance.GetAppSetup();

                if (filter != null)
                {

                }


                ListUsersRequest listUsersRequest = new ListUsersRequest()
                {
                    AttributesToGet = new List<string>() { "email", "name", "family_name", "custom:IsAdmin" },
                    UserPoolId = appSetup.AWSConfig.CognitoConfig.PoolId
                };

                return Task.Run(async () => await cognitoProvider.ListUsersAsync(listUsersRequest)).Result;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
