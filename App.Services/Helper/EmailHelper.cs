﻿using App.Services.Helper.Classes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Mail;
using System.Text;

namespace App.Services.Helper
{
    internal static class EmailHelper
    {
        private static AppSetup appSetup = null;

        static EmailHelper()
        {
            if (appSetup == null)
            {
                appSetup = BaseHeper.AppInstance.GetAppSetup();
            }
        }


        /// <summary>
        /// Get mail message to send
        /// </summary>
        /// <param name="mailReceiverAddress"></param>
        /// <param name="subject"></param>
        /// <param name="body"></param>
        /// <returns></returns>
        private static MailMessage GetMailMessage(string mailReceiverAddress, string subject, string body)
        {
            MailMessage mailMessage = new MailMessage();
            mailMessage.From = new MailAddress(appSetup.MailConfig.Username);
            mailMessage.To.Add(mailReceiverAddress);
            mailMessage.Subject = subject;
            mailMessage.Body = body;
            mailMessage.IsBodyHtml = true;

            return mailMessage;
        }

        public static void SendMail(EmailBody emailBody, string mailReceiverAddress, string subject)
        {
            try
            {
                string mailBodyString = GetEmailBody(emailBody);
                MailMessage mail = GetMailMessage(mailReceiverAddress, subject, mailBodyString);
                System.Threading.Tasks.Task.Run(() => Send(mail));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static void Send(MailMessage mail)
        {

            using (SmtpClient smtpClient = new SmtpClient(appSetup.MailConfig.SmtpClient, appSetup.MailConfig.Port))
            {
                smtpClient.Credentials = new System.Net.NetworkCredential(appSetup.MailConfig.Username, appSetup.MailConfig.Password);
                smtpClient.EnableSsl = true;
                smtpClient.Send(mail);
            }
        }

        private static string GetEmailBody(EmailBody emailBody)
        {
            string mailBodyTemplate = File.ReadAllText(appSetup.AppBasePath + "//" + appSetup.AppConfig.MailTempatesFolder + "//" + Enum.GetName(typeof(MailTemplates), (int)emailBody.MailTemplateIdentifier) + ".html");

            mailBodyTemplate = mailBodyTemplate.Replace("{@@username@@}", emailBody.UserName);
            mailBodyTemplate = mailBodyTemplate.Replace("{@@tempPassword@@}", emailBody.TempPassword);
            mailBodyTemplate = mailBodyTemplate.Replace("{@@appClientUrl@@}", appSetup.AppConfig.ClientAppUrl);

            return mailBodyTemplate;
        }
    }
}
