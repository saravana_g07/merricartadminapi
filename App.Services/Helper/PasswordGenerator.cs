﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace App.Services.Helper
{
    public class PasswordGenerator
    {
        public PasswordGenerator()
        {

        }
        private static PasswordGenerator instance;
        public static PasswordGenerator Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new PasswordGenerator();
                }
                return instance;
            }
        }

        #region password provider

        public Tuple<string, string> Get_Password_and_salt(string userPassword)
        {
            string salt = GenerateSalt();
            string encrytedPassword = GetEncryptedValue(salt, userPassword);
            Tuple<string, string> result = new Tuple<string, string>(encrytedPassword, salt);
            return result;
        }

        public string Retirve_Password(string salt, string inputPassword)
        {
            return GetEncryptedValue(salt, inputPassword);
        }

        #endregion

        #region password Manager

        private string GenerateSalt()
        {
            int length = 32;
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz";
            Random random = new Random();
            return new string(Enumerable.Repeat(chars, length).Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public string GetEncryptedValue(string hash, string plainText)
        {
            byte[] bytesToBeEncrypted = Encoding.UTF8.GetBytes(hash);
            byte[] passwordBytes = Encoding.UTF8.GetBytes(plainText);

            passwordBytes = SHA256.Create().ComputeHash(passwordBytes);

            byte[] bytesEncrypted = AES_Encrypt(bytesToBeEncrypted, passwordBytes);

            string result = Convert.ToBase64String(bytesEncrypted);

            return result;
        }

        private byte[] AES_Encrypt(byte[] bytesToBeEncrypted, byte[] passwordBytes)
        {
            byte[] encryptedBytes = null;
            byte[] saltBytes = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 };
            using (MemoryStream ms = new MemoryStream())
            {
                using (RijndaelManaged AES = new RijndaelManaged())
                {
                    AES.KeySize = 256;
                    AES.BlockSize = 128;

                    var key = new Rfc2898DeriveBytes(passwordBytes, saltBytes, 1000);
                    AES.Key = key.GetBytes(AES.KeySize / 8);
                    AES.IV = key.GetBytes(AES.BlockSize / 8);

                    AES.Mode = CipherMode.CBC;

                    using (var cs = new CryptoStream(ms, AES.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(bytesToBeEncrypted, 0, bytesToBeEncrypted.Length);
                        cs.Close();
                    }
                    encryptedBytes = ms.ToArray();
                }
            }

            return encryptedBytes;
        }

        public string GetRandomString()
        {
            try
            {
                Random random = new Random();
                const string chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                return new string(Enumerable.Repeat(chars, 8).Select(s => s[random.Next(s.Length)]).ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
    }
}
