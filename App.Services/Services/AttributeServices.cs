﻿using App.DAL;
using App.DAL.FilterModel;
using App.DAL.Model;
using App.Services.Helper.Classes;
using App.Services.Services.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace App.Services.Services
{
    public class AttributeServices : IAttributeServices
    {
        private static IAttributeServices _instance = null;
        public static IAttributeServices Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new AttributeServices();
                }
                return _instance;
            }
        }
        public AttributeServices()
        {
        }

        /// <summary>
        /// Get all available attributes
        /// </summary>
        /// <param name="sessionManager.UnitOfWork"></param>
        /// <returns></returns>
        public Tuple<List<Attributes>, int> GetAllAttributes(SessionManager sessionManager, DataTableFilter filter)
        {
            try
            {
                var result = sessionManager.UnitOfWork.attributeRepository.GetAllAttributes(filter);
                return new Tuple<List<Attributes>, int>(result.Item1.ToList(), result.Item2);
            }
            catch (Exception ex)
            {
                return null;
            }
        }/// <summary>
         /// Get all available active attributes
         /// </summary>
         /// <param name="sessionManager.UnitOfWork"></param>
         /// <returns></returns>
        public List<Attributes> GetAllActiveAttributes(SessionManager sessionManager)
        {
            try
            {
                return sessionManager.UnitOfWork.attributeRepository.GetAllActiveAttributes().ToList();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public int InsertAttribute(SessionManager sessionManager, Attributes attribute)
        {
            try
            {
                attribute.Type = "1";
                attribute.CreatedOn = DateTime.Now;
                attribute.LastModifiedOn = DateTime.Now;
                attribute.LastModifiedReason = "New attribute created";

                int attributeId = sessionManager.UnitOfWork.attributeRepository.InsertAttribute(attribute);
                return attributeId;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public bool DeleteAttribute(SessionManager sessionManager, int attributeId)
        {
            try
            {
                Attributes attribute = new Attributes();
                attribute.Id = attributeId;
                attribute.IsDeleted = true;
                attribute.LastModifiedOn = DateTime.Now;
                attribute.LastModifiedReason = "attribute deleted";

                using (var tx = sessionManager.UnitOfWork.BeginTransaction())
                {
                    sessionManager.UnitOfWork.attributeRepository.DeleteAttribute(attribute);
                    tx.Commit();
                }
                return true;
            }
            catch (Exception)
            {
            }
            return false;

        }

        public bool UpdateAttribute(SessionManager sessionManager, Attributes attributeInfo)
        {
            try
            {
                Attributes attribute = new Attributes();
                attribute.Id = attributeInfo.Id;
                attribute.Name = attributeInfo.Name;
                attribute.IsActive = attributeInfo.IsActive;
                attribute.LastModifiedOn = DateTime.Now;
                attribute.LastModifiedReason = "attribute updated";

                using (var tx = sessionManager.UnitOfWork.BeginTransaction())
                {
                    sessionManager.UnitOfWork.attributeRepository.UpdateAttribute(attribute);
                    tx.Commit();
                }
                return true;
            }
            catch (Exception)
            {
            }
            return false;

        }


    }
}
