﻿using App.DAL;
using App.DAL.Model;
using App.DAL.FilterModel;
using App.Services.Services.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using App.Services.Helper.Classes;
using App.Services.Helper.AWSHelper;

namespace App.Services.Services
{
    public class ProductServices : IProductServices
    {

        private static IProductServices _instance = null;
        public static IProductServices Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new ProductServices();
                }
                return _instance;
            }
        }
        public ProductServices()
        {
        }


        /// <summary>
        /// Get product list based on the page number and size of page
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public Tuple<List<IProductList>, int> GetAllProducts(SessionManager sessionManager, DataTableFilter filter)
        {

            try
            {
                var result = sessionManager.UnitOfWork.productRepository.GetAllProducts(filter);
                return new Tuple<List<IProductList>, int>(result.Item1.ToList(), result.Item2);
            }
            catch (Exception)
            {
                //exception or error message will be added here
                return null;
            }
        }

        public List<Products> GetProductByID(SessionManager sessionManager, string id)
        {
            try
            {
                return sessionManager.UnitOfWork.productRepository.GetProductByID(id).ToList();
            }
            catch (Exception)
            {
                //exception or error message will be added here
                return null;
            }
        }

        public Tuple<List<Categories>, List<Attributes>> GetAllCategories(SessionManager sessionManager)
        {

            try
            {
                List<Categories> categoryList = sessionManager.UnitOfWork.productRepository.GetAllCategories().ToList();
                List<Attributes> attrs = sessionManager.UnitOfWork.productRepository.GetAttributes().ToList();
                return Tuple.Create<List<Categories>, List<Attributes>>(categoryList, attrs);
            }
            catch (Exception)
            {
                //exception or error message will be added here
                return null;
            }
        }

        public int CreateNewProduct(SessionManager sessionManager, Products products, List<string> newImageDataUrl)
        {
            int productId = 0;
            try
            {
                #region product Info

                products.IsDeleted = false;
                products.CreatedOn = DateTime.Now;
                products.LastModifiedReason = "New product created";

                #endregion

                using (var tx = sessionManager.UnitOfWork.BeginTransaction())
                {
                    productId = sessionManager.UnitOfWork.productRepository.CreateNewProduct(products);

                    #region Insert product image
                    if (newImageDataUrl != null && newImageDataUrl.Count > 0)
                    {
                        List<string> productImageList = UploadProductImages(newImageDataUrl, productId);
                        sessionManager.UnitOfWork.productRepository.InsertProductImage(productImageList, productId);
                    }

                    #endregion

                    tx.Commit();
                }

            }
            catch (Exception ex)
            {
            }
            return productId;
        }

        private List<string> UploadProductImages(List<string> dataUrlList, int productId)
        {
            try
            {
                List<string> productImageList = new List<string>();

                foreach (string dataUrl in dataUrlList)
                {
                    string productImageName = Guid.NewGuid().ToString() + ".jpeg";
                    S3Helper.UploadDataUrl("products/" + productImageName, dataUrl);
                    productImageList.Add(productImageName);
                }
                return productImageList;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// Delete category
        /// </summary>
        /// <param name="sessionManager.UnitOfWork"></param>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        public bool DeleteProduct(SessionManager sessionManager, int productId)
        {
            try
            {
                Products productInfo = new Products();
                productInfo.Id = productId;
                productInfo.IsDeleted = true;
                productInfo.LastModifiedOn = DateTime.Now;
                productInfo.LastModifiedReason = "product deleted";
                using (var tx = sessionManager.UnitOfWork.BeginTransaction())
                {
                    sessionManager.UnitOfWork.productRepository.DeleteProduct(productInfo);
                    tx.Commit();
                }
                return true;
            }
            catch (Exception)
            {
            }
            return false;

        }

        public bool UpdateProduct(SessionManager sessionManager, Products productInfo, List<string> productImagesToDelete, List<string> newImageDataUrl)
        {
            try
            {
                productInfo.LastModifiedOn = DateTime.Now;
                productInfo.LastModifiedReason = "product updated";

                using (var tx = sessionManager.UnitOfWork.BeginTransaction())
                {
                    sessionManager.UnitOfWork.productRepository.UpdateProduct(productInfo);
                    sessionManager.UnitOfWork.productRepository.DeleteProductImagesById(productImagesToDelete);

                    #region Insert product image
                    if (newImageDataUrl != null & newImageDataUrl.Count > 0)
                    {
                        List<string> productImageList = UploadProductImages(newImageDataUrl, productInfo.Id);
                        sessionManager.UnitOfWork.productRepository.InsertProductImage(productImageList, productInfo.Id);
                    }

                    #endregion


                    tx.Commit();
                }
                return true;
            }
            catch (Exception)
            {
            }
            return false;

        }

        public List<string> GetProductImage(SessionManager sessionManager, int productId)
        {
            try
            {
                return sessionManager.UnitOfWork.productRepository.GetProductImageNameByProdyctId(productId);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

    }
}
