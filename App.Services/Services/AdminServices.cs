﻿using App.DAL.FilterModel;
using App.DAL.Model;
using App.Services.Helper.Classes;
using App.Services.Services.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace App.Services.Services
{

    public class AdminServices : IAdminServices
    {
        private static IAdminServices _instance = null;
        public static IAdminServices Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new AdminServices();
                }
                return _instance;
            }
        }
        public AdminServices()
        {
        }

        public List<RoleInfo> GetAllRoles(SessionManager sessionManager)
        {
            try
            {
                return sessionManager.UnitOfWork.adminRepository.GetAllRoles();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<ModuleInfo> GetAllModules(SessionManager sessionManager)
        {
            try
            {
                return sessionManager.UnitOfWork.adminRepository.GetAllModule().Where(x=>x.URL!=""&&x.URL!=null).ToList();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public Tuple<List<RoleInfo>, int> GetRoleList(SessionManager sessionManager, DataTableFilter filter)
        {
            try
            {
                var result = sessionManager.UnitOfWork.adminRepository.GetRoleList(filter);
                return new Tuple<List<RoleInfo>, int>(result.Item1.ToList(), result.Item2);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<ModuleHierarchy> GetAllModulesHierarchy(SessionManager sessionManager)
        {
            try
            {
                List<ModuleInfo> allModules = sessionManager.UnitOfWork.adminRepository.GetAllModule();

                List<ModuleInfo> accessibleModulesHierarchy = new List<ModuleInfo>(); ;
                foreach (ModuleInfo module in allModules)
                {
                    accessibleModulesHierarchy.Add(module);
                    ModuleInfo moduleInfo = allModules.Where(x => x.Id == module.ParentModuleId).FirstOrDefault();
                    if (moduleInfo != null && !allModules.Contains(moduleInfo))
                    {
                        accessibleModulesHierarchy.Add(moduleInfo);
                    }
                }
                return FormModuleHierarchy(accessibleModulesHierarchy.Distinct().ToList());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private List<ModuleHierarchy> FormModuleHierarchy(List<ModuleInfo> accessibleModules)
        {
            try
            {
                List<ModuleHierarchy> moduleHierarchyList = new List<ModuleHierarchy>();
                foreach (ModuleInfo module in accessibleModules.Where(x => x.ParentModuleId == 0).ToList())
                {
                    List<ModuleInfo> children = accessibleModules.Where(x => x.ParentModuleId == module.Id).ToList();
                    moduleHierarchyList.Add(new ModuleHierarchy(module.Id, module.ModuleName, module.ParentModuleId, module.URL, module.Icon, children));
                }
                return moduleHierarchyList;
            }
            catch (Exception ex)
            {
                return null;
            }
        }




        /// <summary>
        /// Create new Role
        /// </summary>
        /// <param name="sessionManager.UnitOfWork"></param>
        /// <param name="categories"></param>
        /// <returns></returns>
        public int CreateRole(SessionManager sessionManager, SystemRole role)
        {
            int roleId = 0;
            try
            {               
                role.IsDeleted = false;
                role.CreatedOn = DateTime.Now;
                role.LastModifiedReason = "New Role created";

                using (var tx = sessionManager.UnitOfWork.BeginTransaction())
                {
                    roleId = sessionManager.UnitOfWork.adminRepository.CreateRole(role);
                    tx.Commit();
                }
            }
            catch (Exception ex)
            {

            }
            return roleId;
        }

        /// <summary>
        /// Delete Role
        /// </summary>
        /// <param name="sessionManager.UnitOfWork"></param>
        /// <param name="RoleId"></param>
        /// <returns></returns>
        public bool DeleteRole(SessionManager sessionManager, int roleId)
        {
            try
            {
                SystemRole role = new SystemRole();
                role.Id = roleId;
                role.IsDeleted = true;
                role.LastModifiedOn = DateTime.Now;
                role.LastModifiedReason = "Role deleted";

                using (var tx = sessionManager.UnitOfWork.BeginTransaction())
                {
                    sessionManager.UnitOfWork.adminRepository.DeleteRole(role);
                    tx.Commit();
                }
                return true;
            }
            catch (Exception)
            {
            }
            return false;

        }

        /// <summary>
        /// update Role
        /// </summary>
        /// <param name="sessionManager.UnitOfWork"></param>
        /// <param name="RoleInfo"></param>
        /// <returns></returns>
        public bool UpdateRole(SessionManager sessionManager, SystemRole oleInfo)
        {
            try
            {
                SystemRole role = new SystemRole();
                role.Id = oleInfo.Id;
                role.RoleName = oleInfo.RoleName;
                role.AccessibleModules = oleInfo.AccessibleModules;
                role.LastModifiedOn = DateTime.Now;
                role.LastModifiedReason = "Role updated";               

                using (var tx = sessionManager.UnitOfWork.BeginTransaction())
                {
                    sessionManager.UnitOfWork.adminRepository.UpdateRole(role);
                    tx.Commit();
                }
                return true;
            }
            catch (Exception)
            {
            }
            return false;

        }


    }
}
