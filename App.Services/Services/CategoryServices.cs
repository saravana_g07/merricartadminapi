﻿using App.DAL;
using App.DAL.Model;
using App.Services.DTO.Model;
using App.Services.Helper.AWSHelper;
using App.Services.Helper.Classes;
using App.Services.Services.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace App.Services.Services
{
    public class CategoryServices : ICategoryServices
    {
        private static ICategoryServices _instance = null;
        public static ICategoryServices Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new CategoryServices();
                }
                return _instance;
            }
        }
        public CategoryServices()
        {
        }

        /// <summary>
        /// Get all available categories in hierarchy
        /// </summary>
        /// <param name="sessionManager.UnitOfWork"></param>
        /// <returns></returns>
        public List<Categorieshierarchy> GetAllCategoriesInHierarchy(SessionManager sessionManager)
        {
            try
            {
                List<Categorieshierarchy> result = new List<Categorieshierarchy>();
                List<Categories> CategoriesList = sessionManager.UnitOfWork.categoriesRepository.GetAllCategories().ToList();
                return FormCategoriesInHierarchy(CategoriesList, result, 0);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        private List<Categorieshierarchy> FormCategoriesInHierarchy(List<Categories> categoriesList, List<Categorieshierarchy> result, int parentId)
        {
            List<Categories> Categories = categoriesList.Where(x => x.ParentId == parentId).ToList();
            foreach (Categories item in Categories)
            {
                Categorieshierarchy categoryInfo = new Categorieshierarchy();
                categoryInfo.Id = item.Id;
                categoryInfo.Name = item.Name;
                categoryInfo.ParentId = item.ParentId;
                categoryInfo.Checked = item.IsActive;
                categoryInfo.AttributeIds = item.AttributeIds;
                categoryInfo.CategoryImage = item.CategoryImage;
                categoryInfo.Children = new List<Categorieshierarchy>();
                categoryInfo.Children = FormCategoriesInHierarchy(categoriesList, categoryInfo.Children, item.Id);
                result.Add(categoryInfo);
            }
            return result;

        }

        /// <summary>
        /// Create new category
        /// </summary>
        /// <param name="sessionManager.UnitOfWork"></param>
        /// <param name="categories"></param>
        /// <returns></returns>
        public int CreateNewCategory(SessionManager sessionManager, Categories categories)
        {
            int categoryId = 0;
            try
            {
                categories.CategoryImage = Guid.NewGuid().ToString() + ".jpeg";
                if (categories.ImageDataUrl == null || categories.ImageDataUrl == "")
                {
                    categories.CategoryImage = "no-image.jpg";
                }
                categories.IsDeleted = false;
                categories.CreatedOn = DateTime.Now;
                categories.LastModifiedReason = "New category created";


                using (var tx = sessionManager.UnitOfWork.BeginTransaction())
                {
                    categoryId = sessionManager.UnitOfWork.categoriesRepository.InsertCategory(categories);
                    tx.Commit();
                }

                S3Helper.UploadDataUrl("categories/" + categories.CategoryImage, categories.ImageDataUrl);

            }
            catch (Exception ex)
            {

            }
            return categoryId;
        }

        /// <summary>
        /// Delete category
        /// </summary>
        /// <param name="sessionManager.UnitOfWork"></param>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        public bool DeleteCategogy(SessionManager sessionManager, int categoryId)
        {
            try
            {
                Categories categories = new Categories();
                categories.Id = categoryId;
                categories.IsDeleted = true;
                categories.LastModifiedOn = DateTime.Now;
                categories.LastModifiedReason = "category deleted";

                using (var tx = sessionManager.UnitOfWork.BeginTransaction())
                {
                    sessionManager.UnitOfWork.categoriesRepository.DeleteCategory(categories);
                    tx.Commit();
                }
                return true;
            }
            catch (Exception)
            {
            }
            return false;

        }

        /// <summary>
        /// update category
        /// </summary>
        /// <param name="sessionManager.UnitOfWork"></param>
        /// <param name="categoryInfo"></param>
        /// <returns></returns>
        public bool UpdateCategory(SessionManager sessionManager, Categories categoryInfo)
        {
            try
            {
                Categories categories = new Categories();
                categories.Id = categoryInfo.Id;
                categories.Name = categoryInfo.Name;
                categories.IsActive = categoryInfo.IsActive;
                categories.AttributeIds = categoryInfo.AttributeIds;
                categories.CategoryImage = categoryInfo.CategoryImage;
                categories.LastModifiedOn = DateTime.Now;
                categories.LastModifiedReason = "category updated";
                if (categoryInfo.ImageDataUrl != null && categoryInfo.ImageDataUrl.Trim().Length > 0)
                {
                    categories.CategoryImage = Guid.NewGuid().ToString() + ".jpeg";
                    S3Helper.UploadDataUrl("categories/" + categories.CategoryImage, categoryInfo.ImageDataUrl);
                }

                using (var tx = sessionManager.UnitOfWork.BeginTransaction())
                {
                    sessionManager.UnitOfWork.categoriesRepository.UpdateCategory(categories);
                    tx.Commit();
                }
                return true;
            }
            catch (Exception)
            {
            }
            return false;

        }
     
        /// <summary>
        /// Set Active/Non-Active categories
        /// </summary>
        /// <param name="sessionManager.UnitOfWork"></param>
        /// <param name="nonActiveCategoryIds"></param>
        /// <returns></returns>
        public bool SetActiveCategory(SessionManager sessionManager, List<int> nonActiveCategoryIds)
        {
            try
            {
                using (var tx = sessionManager.UnitOfWork.BeginTransaction())
                {
                    sessionManager.UnitOfWork.categoriesRepository.SetActiveCategories(nonActiveCategoryIds);
                    tx.Commit();
                }
                return true;
            }
            catch (Exception)
            {
            }
            return false;
        }

    }
}
