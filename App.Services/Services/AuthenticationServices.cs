﻿using App.DAL;
using App.DAL.Model;
using App.Services.Helper;
using App.Services.Helper.Classes;
using App.Services.Services.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace App.Services.Services
{
    public class AuthenticationServices : IAuthenticationServices
    {

        #region Initialize

        private static IAuthenticationServices _instance = null;

        public static IAuthenticationServices Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new AuthenticationServices();
                }
                return _instance;
            }
        }
        public AuthenticationServices()
        {
        }

        #endregion
        public Session LogInUser(SessionManager sessionManager, string loginEmail, string password)
        {
            try
            {
                IUserInfo user = sessionManager.UnitOfWork.userRepository.GetUserByEmailId(loginEmail);

                if (user != null && user.IsActive)
                {
                    //var authResponse = CognitoHelper.Instance.SignInUser(loginEmail, password);
                    //return new Session
                    //{
                    //    SessionToken = authResponse.AuthenticationResult,
                    //    User = user,
                    //    IdleLogoutTime = BaseHeper.AppInstance.GetAppSetup().UserConfig.IdleLogoutTimeInMinutes
                    //};

                    PasswordInfo userPassword = sessionManager.UnitOfWork.userRepository.GetUserPassword(loginEmail).FirstOrDefault();
                    string passwordHash = PasswordGenerator.Instance.Retirve_Password(userPassword.Salt, password);
                    if (passwordHash == userPassword.Password)
                    {
                        List<int> roleIds = user.UserRole.Split(',').Select(s => int.Parse(s)).ToList();
                        List<ModuleHierarchy> moduleHierarchies = GetAccessibleModules(sessionManager, roleIds);
                        return CreateSession(user, moduleHierarchies);
                    }
                }

                throw new MemberAccessException("Incorrect username or password.!");
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null && ((Amazon.Runtime.AmazonServiceException)ex.InnerException).ErrorCode == "NotAuthorizedException")
                {
                    throw new MemberAccessException("Incorrect username or password.!");
                }
                throw ex;
            }
        }

        public bool LogoutUser(string sessionToken)
        {
            try
            {
                //CognitoHelper.Instance.SignOutUser(accessToken);

                return AppCacheHelper<bool>.DeleteCache(sessionToken);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public bool ResetPassword(SessionManager sessionManager, ResetPassword userPassword)
        {
            try
            {
                if (IsValidUser(sessionManager, userPassword.LoginEmail, userPassword.CurrentPassword))
                {
                    Tuple<string, string> newPasswordSalt = PasswordGenerator.Instance.Get_Password_and_salt(userPassword.NewPassword);

                    User user = new User();
                    user.LoginEmail = userPassword.LoginEmail;
                    user.Password = newPasswordSalt.Item1;
                    user.Salt = newPasswordSalt.Item2;
                    user.LastModifiedOn = DateTime.Now;
                    user.LastModifiedReason = "Password reset";

                    using (var tx = sessionManager.UnitOfWork.BeginTransaction())
                    {
                        sessionManager.UnitOfWork.userRepository.UpdateUserPassword(user);
                        tx.Commit();
                    }

                    return true;
                }
                throw new MemberAccessException("Incorrect username or password.!");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ForgotPassword(SessionManager sessionManager, string loginEmail)
        {
            try
            {
                string userName = sessionManager.UnitOfWork.userRepository.GetUserNameByEmailId(loginEmail);
                if (userName == null || userName.Trim().Length < 1)
                {
                    return false;
                }

                string tempPassword = PasswordGenerator.Instance.GetRandomString();
                Tuple<string, string> newPasswordSalt = PasswordGenerator.Instance.Get_Password_and_salt(tempPassword);

                User user = new User();
                user.LoginEmail = loginEmail;
                user.Password = newPasswordSalt.Item1;
                user.Salt = newPasswordSalt.Item2;
                user.LastModifiedOn = DateTime.Now;
                user.LastModifiedReason = "Forgot password reset";

                using (var tx = sessionManager.UnitOfWork.BeginTransaction())
                {
                    sessionManager.UnitOfWork.userRepository.UpdateUserPassword(user);
                    tx.Commit();
                }

                #region Send Temp password & reset password link

                EmailBody emailBody = new EmailBody();
                emailBody.MailTemplateIdentifier = MailTemplates.ForgorPasswordTemplate;
                emailBody.UserName = userName;
                emailBody.TempPassword = tempPassword;

                EmailHelper.SendMail(emailBody, loginEmail, "Reset password link");

                #endregion

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private bool IsValidUser(SessionManager sessionManager, string loginEmail, string plainPassword)
        {
            try
            {
                IPassword password = sessionManager.UnitOfWork.userRepository.GetUserPassword(loginEmail).FirstOrDefault();
                string currentPassword = PasswordGenerator.Instance.Retirve_Password(password.Salt, plainPassword);
                if (password.Password == currentPassword)
                {
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private Session CreateSession(IUserInfo userInfo, List<ModuleHierarchy> moduleHierarchies = null)
        {
            string sessionId = PasswordGenerator.Instance.GetEncryptedValue(DateTime.Now.Ticks.ToString(), Guid.NewGuid().ToString());
            Session session = new Session
            {
                SessionToken = sessionId,
                User = userInfo as UserInfo,
                IdleLogoutTime = BaseHeper.AppInstance.GetAppSetup().UserConfig.IdleLogoutTimeInMinutes,
                ModuleHierarchy = moduleHierarchies
            };
            AppCacheHelper<Session>.SetCache(session, sessionId);

            return session;
        }

        public Session IsValidSession(string sessionToken)
        {
            try
            {
                Session session = AppCacheHelper<Session>.GetCache(sessionToken);
                if (session != null && session.SessionToken == sessionToken)
                {
                    return session;
                }
                throw new UnauthorizedAccessException("Invalid user");
            }
            catch
            {
                throw new UnauthorizedAccessException("Invalid user!");
            }

        }

        private List<ModuleHierarchy> GetAccessibleModules(SessionManager sessionManager, List<int> roleIds)
        {
            try
            {
                List<ModuleInfo> allModules = sessionManager.UnitOfWork.adminRepository.GetAllModule();
                List<ModuleInfo> accessibleModules = new List<ModuleInfo>();

                if (!roleIds.Contains((int)SystemRoles.SuperAdmin) && !roleIds.Contains((int)SystemRoles.Admin))
                {
                    List<int> moduleIdList = sessionManager.UnitOfWork.adminRepository.GetAccessibleModuleIdByRole(roleIds);
                    accessibleModules = allModules.Where(x => moduleIdList.Contains(x.Id)).ToList();
                }
                else
                {
                    accessibleModules = allModules;
                }

                List<ModuleInfo> accessibleModulesHierarchy = new List<ModuleInfo>(); ;
                foreach (ModuleInfo module in accessibleModules)
                {
                    accessibleModulesHierarchy.Add(module);
                    ModuleInfo moduleInfo = allModules.Where(x => x.Id == module.ParentModuleId).FirstOrDefault();
                    if (moduleInfo != null && !accessibleModules.Contains(moduleInfo))
                    {
                        accessibleModulesHierarchy.Add(moduleInfo);
                    }
                }
                return FormModuleHierarchy(accessibleModulesHierarchy.Distinct().ToList());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private List<ModuleHierarchy> FormModuleHierarchy(List<ModuleInfo> accessibleModules)
        {
            try
            {
                List<ModuleHierarchy> moduleHierarchyList = new List<ModuleHierarchy>();
                foreach (ModuleInfo module in accessibleModules.Where(x => x.ParentModuleId == 0).ToList())
                {
                    List<ModuleInfo> children = accessibleModules.Where(x => x.ParentModuleId == module.Id).ToList();
                    moduleHierarchyList.Add(new ModuleHierarchy(module.Id, module.ModuleName, module.ParentModuleId, module.URL, module.Icon, children));
                }
                return moduleHierarchyList;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

    }
}