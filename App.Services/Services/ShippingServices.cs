﻿using App.DAL.Model;
using App.Services.Helper.Classes;
using App.Services.Services.IServices;
using System;
using System.Collections.Generic;
using System.Text;

namespace App.Services.Services
{
    public class ShippingServices : IShippingServices
    {
        private static IShippingServices _instance = null;

        public static IShippingServices Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new ShippingServices();
                }
                return _instance;
            }
        }
        public ShippingServices()
        {
        }   
    }
}
