﻿using App.DAL;
using App.DAL.FilterModel;
using App.DAL.Model;
using App.Services.DTO.Model;
using App.Services.Helper;
using App.Services.Helper.AWSHelper;
using App.Services.Helper.Classes;
using App.Services.Services.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace App.Services.Services
{
    public class UserServices : IUserServices
    {
        private static IUserServices _instance = null;

        public static IUserServices Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new UserServices();
                }
                return _instance;
            }
        }
        public UserServices()
        {
        }

        public Tuple<List<IUserInfo>, int> GetAllUsers(SessionManager sessionManager, DataTableFilter filter)
        {
            try
            {
                //CognitoHelper.Instance.GetListOfUsers(filter);
                var result = sessionManager.UnitOfWork.userRepository.GetUserList(filter);
                return new Tuple<List<IUserInfo>, int>(result.Item1.ToList(), result.Item2);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public int InsertUser(SessionManager sessionManager, User user)
        {
            try
            {
                string tempPassword = PasswordGenerator.Instance.GetRandomString();
                Tuple<string, string> encryptedPassword = PasswordGenerator.Instance.Get_Password_and_salt(tempPassword);
                //CognitoHelper.Instance.SignUpUser(user);

                user.Password = encryptedPassword.Item1;
                user.Salt = encryptedPassword.Item2;
                user.UserImage = "no-user.png";
                user.RecentLogin = default(DateTime);
                user.CreatedOn = DateTime.Now;
                user.LastModifiedOn = DateTime.Now;
                user.LastModifiedReason = "New user created";

                int userId = sessionManager.UnitOfWork.userRepository.InsertUser(user);

                #region Send Account created email

                EmailBody emailBody = new EmailBody();
                emailBody.MailTemplateIdentifier = MailTemplates.NewUserTemplate;
                emailBody.UserName = user.FirstName + " " + user.LastName;
                emailBody.TempPassword = tempPassword;

                EmailHelper.SendMail(emailBody, user.LoginEmail, "Account created");

                #endregion

                return userId;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public bool DeleteUser(SessionManager sessionManager, int userId)
        {
            try
            {
                User user = new User();
                user.Id = userId;
                user.IsDeleted = true;
                user.LastModifiedOn = DateTime.Now;
                user.LastModifiedReason = "category deleted";

                using (var tx = sessionManager.UnitOfWork.BeginTransaction())
                {
                    sessionManager.UnitOfWork.userRepository.DeleteUser(user);
                    tx.Commit();
                }
                return true;
            }
            catch (Exception)
            {
            }
            return false;

        }

        public bool UpdateUser(SessionManager sessionManager, User userInfo)
        {
            try
            {
                User user = new User();
                user.Id = userInfo.Id;
                user.FirstName = userInfo.FirstName;
                user.LastName = userInfo.LastName;
                user.IsActive = userInfo.IsActive;
                user.UserRole = userInfo.UserRole;
                user.LoginEmail = userInfo.LoginEmail;
                user.LastModifiedOn = DateTime.Now;
                user.LastModifiedReason = "user updated";

                using (var tx = sessionManager.UnitOfWork.BeginTransaction())
                {
                    sessionManager.UnitOfWork.userRepository.UpdateUser(user);
                    tx.Commit();
                }
                return true;
            }
            catch (Exception)
            {
            }
            return false;

        }

        public bool UpdateMyProfile(SessionManager sessionManager, User userInfo)
        {
            try
            {
                User user = new User();
                user.Id = sessionManager.Session.User.Id;
                user.FirstName = userInfo.FirstName;
                user.LastName = userInfo.LastName; ;
                user.LastModifiedOn = DateTime.Now;
                user.LastModifiedReason = "user updated";

                using (var tx = sessionManager.UnitOfWork.BeginTransaction())
                {
                    sessionManager.UnitOfWork.userRepository.UpdateMyProfile(user);
                    tx.Commit();
                }
                return true;
            }
            catch (Exception)
            {
            }
            return false;

        }

        public string UpdateUserImage(SessionManager sessionManager, string dataUrl)
        {
            try
            {
                string userImageId = Guid.NewGuid().ToString() + ".jpeg";
                bool isuploaded = S3Helper.UploadDataUrl("user/" + userImageId, dataUrl);
                if (!isuploaded)
                {
                    throw new Exception("Failed to update Image!");
                }

                User user = new User();
                user.Id = sessionManager.Session.User.Id;
                user.UserImage = sessionManager.Session.User.UserImage = userImageId;
                user.LastModifiedOn = DateTime.Now;
                user.LastModifiedReason = "Profile picture changed";

                using (var tx = sessionManager.UnitOfWork.BeginTransaction())
                {
                    sessionManager.UnitOfWork.userRepository.UpdateUserImage(user);
                    tx.Commit();
                }

                return userImageId;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
