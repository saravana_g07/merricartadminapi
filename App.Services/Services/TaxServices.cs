﻿using App.Services.Services.IServices;
using System;
using System.Collections.Generic;
using System.Text;

namespace App.Services.Services
{
    public class TaxServices : ITaxServices
    {
        private static ITaxServices _instance = null;

        public static ITaxServices Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new TaxServices();
                }
                return _instance;
            }
        }
        public TaxServices()
        {
        }
    }
}
