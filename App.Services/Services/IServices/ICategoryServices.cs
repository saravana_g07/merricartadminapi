﻿using App.DAL;
using App.DAL.Model;
using App.Services.DTO.Model;
using App.Services.Helper.Classes;
using System;
using System.Collections.Generic;
using System.Text;

namespace App.Services.Services.IServices
{
    public interface ICategoryServices
    {
        /// <summary>
        /// Get all available categories in hierarchy
        /// </summary>
        /// <param name="sessionManager.UnitOfWork"></param>
        /// <returns></returns>
        List<Categorieshierarchy> GetAllCategoriesInHierarchy(SessionManager sessionManager);

        /// <summary>
        /// Create new category
        /// </summary>
        /// <param name="sessionManager.UnitOfWork"></param>
        /// <param name="categories"></param>
        /// <returns></returns>
        int CreateNewCategory(SessionManager sessionManager, Categories categories);

        /// <summary>
        /// Delete category
        /// </summary>
        /// <param name="sessionManager.UnitOfWork"></param>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        bool DeleteCategogy(SessionManager sessionManager, int categoryId);

        /// <summary>
        /// update category
        /// </summary>
        /// <param name="sessionManager.UnitOfWork"></param>
        /// <param name="categoryInfo"></param>
        /// <returns></returns>
        bool UpdateCategory(SessionManager sessionManager, Categories categoryInfo);

        bool SetActiveCategory(SessionManager sessionManager, List<int> nonActiveCategoryIds);
    }
}
