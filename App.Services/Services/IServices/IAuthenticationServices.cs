﻿using App.DAL;
using App.DAL.Model;
using App.Services.Helper.Classes;
using System;
using System.Collections.Generic;
using System.Text;

namespace App.Services.Services.IServices
{
    public interface IAuthenticationServices
    {
        Session LogInUser(SessionManager sessionManager, string loginEmail, string password);
        bool LogoutUser(string accessToken);
        bool ResetPassword(SessionManager sessionManager, ResetPassword userPassword);
        bool ForgotPassword(SessionManager sessionManager, string loginEmail);
        Session IsValidSession(string sessionToken);
    }
}
