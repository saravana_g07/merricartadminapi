﻿using App.DAL.FilterModel;
using App.DAL.Model;
using App.Services.Helper.Classes;
using System;
using System.Collections.Generic;
using System.Text;

namespace App.Services.Services.IServices
{
    public interface IAdminServices
    {
        List<RoleInfo> GetAllRoles(SessionManager sessionManager);
        List<ModuleInfo> GetAllModules(SessionManager sessionManager);
        List<ModuleHierarchy> GetAllModulesHierarchy(SessionManager sessionManager);
        Tuple<List<RoleInfo>, int> GetRoleList(SessionManager sessionManager, DataTableFilter filter);
        int CreateRole(SessionManager sessionManager, SystemRole role);
        bool DeleteRole(SessionManager sessionManager, int roleId);
        bool UpdateRole(SessionManager sessionManager, SystemRole role);
    }
}
