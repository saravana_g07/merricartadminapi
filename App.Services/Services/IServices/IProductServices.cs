﻿using App.DAL;
using App.DAL.Model;
using App.DAL.FilterModel;
using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using App.Services.Helper.Classes;

namespace App.Services.Services.IServices
{
    public interface IProductServices
    {
        /// <summary>
        /// Get product list based on the page number and size of page
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Tuple<List<IProductList>, int> GetAllProducts(SessionManager sessionManager, DataTableFilter filter);

        List<Products> GetProductByID(SessionManager sessionManager, string id);

        Tuple<List<Categories>, List<Attributes>> GetAllCategories(SessionManager sessionManager);
        /// <summary>
        /// Create new Product
        /// </summary>
        /// <param name="sessionManager.UnitOfWork"></param>
        /// <param name="products"></param>
        /// <returns></returns>
        int CreateNewProduct(SessionManager sessionManager, Products products, List<string> newImageDataUrl);

        /// <summary>
        /// Delete Product
        /// </summary>
        /// <param name="sessionManager.UnitOfWork"></param>
        /// <param name="productid"></param>
        /// <returns></returns>
        bool DeleteProduct(SessionManager sessionManager, int productId);

        /// <summary>
        /// update Product
        /// </summary>
        /// <param name="sessionManager.UnitOfWork"></param>
        /// <param name="productInfo"></param>
        /// <returns></returns>
        bool UpdateProduct(SessionManager sessionManager, Products productInfo, List<string> productImagesToDelete, List<string> newImageDataUrl);

        /// <summary>
        /// Get product image name list by product id
        /// </summary>
        /// <param name="sessionManager"></param>
        /// <param name="productId"></param>
        /// <returns></returns>
        List<string> GetProductImage(SessionManager sessionManager, int productId);
    }
}
