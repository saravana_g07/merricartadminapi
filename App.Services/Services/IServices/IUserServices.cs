﻿using App.DAL;
using App.DAL.FilterModel;
using App.DAL.Model;
using App.Services.DTO.Model;
using App.Services.Helper.Classes;
using System;
using System.Collections.Generic;
using System.Text;

namespace App.Services.Services.IServices
{
    public interface IUserServices
    {
        Tuple<List<IUserInfo>, int> GetAllUsers(SessionManager sessionManager, DataTableFilter filter);
        int InsertUser(SessionManager sessionManager, User user);
        bool DeleteUser(SessionManager sessionManager, int userId);
        bool UpdateUser(SessionManager sessionManager, User userInfo);
        bool UpdateMyProfile(SessionManager sessionManager, User userInfo);
        string UpdateUserImage(SessionManager sessionManager, string dataUrl);
    }
}
