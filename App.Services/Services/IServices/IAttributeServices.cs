﻿using App.DAL;
using App.DAL.FilterModel;
using App.DAL.Model;
using App.Services.Helper.Classes;
using System;
using System.Collections.Generic;
using System.Text;

namespace App.Services.Services.IServices
{
    public interface IAttributeServices
    {
        Tuple<List<Attributes>, int> GetAllAttributes(SessionManager sessionManager, DataTableFilter filter);
        List<Attributes> GetAllActiveAttributes(SessionManager sessionManager);
        int InsertAttribute(SessionManager sessionManager, Attributes attribute);
        bool DeleteAttribute(SessionManager sessionManager, int attributeId);
        bool UpdateAttribute(SessionManager sessionManager, Attributes attributeInfo);
    }
}