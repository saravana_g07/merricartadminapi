﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App.Services.DTO.Model.Base
{
    public abstract class BaseDTOResponse
    {
        public string ErrorMessage { get; set; }
        public string WarningMessage { get; set; }
    }
}
