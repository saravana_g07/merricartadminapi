﻿using ServiceStack;
using ServiceStack.Web;
using System;
using System.Collections.Generic;
using System.Text;

namespace App.Services.DTO.Model
{
    public class DataTableResponse: HttpResult
    {
        public int RecordsTotal { get; set; }
        public int RecordsFiltered { get; set; }
        public dynamic Data { get; set; }

        public DataTableResponse()
        {

        }
        public DataTableResponse(int recordsTotal, int recordFiltered, dynamic data)
        {
            this.RecordsTotal = recordsTotal;
            this.RecordsFiltered = recordFiltered;
            this.Data = data;
        }
    }

}
