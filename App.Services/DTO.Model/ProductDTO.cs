﻿using App.DAL.Model;
using App.Services.DTO.Model.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace App.Services.DTO.Model
{
    public class ProductDTO : BaseDTOResponse
    {
        Products Product { get; set; }
        List<int> ProductImagesToDelete { get; set; }
        List<string> ProductDataUrlList { get; set; }
    }
}
