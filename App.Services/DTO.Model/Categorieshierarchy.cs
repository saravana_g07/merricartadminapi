﻿using App.Services.DTO.Model.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace App.Services.DTO.Model
{
    public class Categorieshierarchy
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int ParentId { get; set; }
        public bool Checked { get; set; }
        public string AttributeIds { get; set; }
        public string CategoryImage { get; set; }

        public List<Categorieshierarchy> Children { get; set; }
    }
}
