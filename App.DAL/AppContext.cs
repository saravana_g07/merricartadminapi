﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace App.DAL
{
    public class AppContext : IAppContext
    {
        public IDbConnection _connection { get; set; }
        public IDbTransaction _transaction { get; set; }
        public AppContext(IDbConnection connection, IDbTransaction transaction)
        {
            _connection = connection;
            _transaction = transaction;
        }

    }
    public interface IAppContext
    {
        IDbConnection _connection { get; set; }
        IDbTransaction _transaction { get; set; }
    }
}
