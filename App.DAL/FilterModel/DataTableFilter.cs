﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App.DAL.FilterModel
{
    public class DataTableFilter
    {
        public int SortingColumnIndex { get; set; }
        public string OrderBy { get; set; }
        public string SearchText { get; set; }
        public int Start { get; set; }
        public int Length { get; set; }

    }
}
