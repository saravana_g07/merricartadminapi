﻿using App.DAL.Model.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace App.DAL.Model
{
    [Table("Country")]
    public class Country : BaseModel
    {
        public static string Table => "Country";
        public int Id { get; set; }
        public string CountryName { get; set; }
        public bool IsActive { get; set; }
    }
}
