﻿using App.DAL.Model.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace App.DAL.Model
{
    [Table("Users")]
    public class User : BaseModel, IPassword
    {
        public static string Table => "Users";
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string LoginEmail { get; set; }
        public string Password { get; set; }
        public string Salt { get; set; }
        public string UserRole { get; set; }
        public bool IsActive { get; set; }
        public DateTime RecentLogin { get; set; }
        private string _userImage { get; set; }
        public string UserImage { get; set; }
    }
    public interface IUserInfo
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string LoginEmail { get; set; }
        public string UserRole { get; set; }
        public string RoleName { get; set; }
        public bool IsActive { get; set; }
        public string UserImage { get; set; }
        public DateTime RecentLogin { get; set; }
    }
    public class UserInfo : IUserInfo
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string LoginEmail { get; set; }
        public string UserRole { get; set; }
        public string RoleName { get; set; }
        public bool IsActive { get; set; }
        public DateTime RecentLogin { get; set; }
        private string _userImage { get; set; }
        public string UserImage { get; set; }
    }
    public interface IPassword
    {
        public string Password { get; set; }
        public string Salt { get; set; }
    }

    public class PasswordInfo : IPassword
    {
        public string Password { get; set; }
        public string Salt { get; set; }
    }

    public class ResetPassword
    {
        public string LoginEmail { get; set; }
        public string CurrentPassword { get; set; }
        public string NewPassword { get; set; }
    }
}