﻿using App.DAL.Model.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace App.DAL.Model
{

    [Table("systemmodules")]
    public class SystemModule : BaseModel
    {
        public static string Table => "systemmodules";
        public int Id { get; set; }
        public string ModuleName { get; set; }
        public int ParentModuleId { get; set; }
        public string URL { get; set; }
        public string Icon { get; set; }
    }

    public class ModuleInfo
    {
        public int Id { get; set; }
        public string ModuleName { get; set; }
        public int ParentModuleId { get; set; }
        public string ParentModuleName { get; set; }
        public string URL { get; set; }
        public string Icon { get; set; }
    }

    public class ModuleHierarchy
    {
        public ModuleHierarchy()
        {

        }
        public ModuleHierarchy(int id, string moduleName, int parentModuleId, string url, string icon, List<ModuleInfo> children)
        {
            this.Id = id;
            this.ModuleName = moduleName;
            this.ParentModuleId = parentModuleId;
            this.URL = url;
            this.Icon = icon;
            this.Children = children;
        }
        public int Id { get; set; }
        public string ModuleName { get; set; }
        public int ParentModuleId { get; set; }
        public string URL { get; set; }
        public string Icon { get; set; }
        public List<ModuleInfo> Children { get; set; }
    }
}
