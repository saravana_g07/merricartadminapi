﻿using App.DAL.Model.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace App.DAL.Model
{
    [Table("systemroles")]
    public class SystemRole : BaseModel
    {
        public static string Table => "systemroles";
        public int Id { get; set; }
        public string RoleName { get; set; }
        public bool IsSystemDefined { get; set; }
        public string AccessibleModules { get; set; }
    }

    public class RoleInfo
    {

        public int Id { get; set; }
        public string RoleName { get; set; }
        public bool IsSystemDefined { get; set; }
        public string AccessibleModules { get; set; }
        public string ModuleName { get; set; }
    }
}
