﻿using App.DAL.Model.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace App.DAL.Model
{
    [Table("Categories")]
    public class Categories : BaseModel
    {
        public static string Table => "Categories";
        public int Id { get; set; }
        public string Name { get; set; }
        public string AttributeIds { get; set; }
        public int ParentId { get; set; }
        public bool IsActive { get; set; }
        public string CategoryImage { get; set; }
        public string ImageDataUrl { get; set; } //not as a table column
    }
}
