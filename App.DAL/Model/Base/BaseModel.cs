﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App.DAL.Model.Base
{
    public class BaseModel
    {
        public bool IsDeleted { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime LastModifiedOn { get; set; }
        public string LastModifiedReason { get; set; }
    }
}
