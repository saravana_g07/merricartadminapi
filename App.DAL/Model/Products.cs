﻿using App.DAL.Model.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace App.DAL.Model
{
    [Table("products")]
    public class Products : BaseModel
    {
        public static string Table => "products";
        public int Id { get; set; }
        public string Name { get; set; }
        public string Category { get; set; }
        public float ActualPrice { get; set; }
        public float SpecialPrice { get; set; }
        public string Sku { get; set; }
        public float Quantity { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
        public List<Dictionary<string, string>> CategoryAttributes { get; set; }
        
    }

    public interface IProductList
    {
        int Id { get; set; }
        string Name { get; set; }
        public string ProductImage { get; set; }
        string Category { get; set; }
        float ActualPrice { get; set; }
        float SpecialPrice { get; set; }
        string Sku { get; set; }
        float Quantity { get; set; }
        DateTime DateFrom { get; set; }
        DateTime DateTo { get; set; }

    }

    public class ProductList : IProductList
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ProductImage { get; set; }
        public string Category { get; set; }
        public float ActualPrice { get; set; }
        public float SpecialPrice { get; set; }
        public string Sku { get; set; }
        public float Quantity { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }

    }
}
