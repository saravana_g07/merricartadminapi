﻿using App.DAL.Model.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace App.DAL.Model
{
    [Table("Attributes")]
    public class Attributes : BaseModel
    {
        public static string Table => "Attributes";
        public int Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public bool IsActive { get; set; }
    }
}
