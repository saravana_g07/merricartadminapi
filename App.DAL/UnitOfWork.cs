﻿using App.DAL.Repositories;
using App.DAL.Repositories.Interface;
using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace App.DAL
{
    public class UnitOfWork : IUnitOfWork
    {

        #region UnitOfWork base actions 

        private IDbConnection _connection = null;
        private IDbTransaction _transaction = null;
        private bool _disposed;
        IConfiguration _iconfiguration;

        public UnitOfWork(IConfiguration iconfiguration)
        {
            _iconfiguration = iconfiguration;
            string connectionString = _iconfiguration.GetSection("ConnectionStrings").GetSection("MerricartContext").Value; ;

            InitializeConnection(connectionString);
        }

        private void InitializeConnection(string connectionString)
        {
            _connection = new MySqlConnection(connectionString);
            _connection.Open();
        }

        public IDbTransaction BeginTransaction()
        {
            if (_transaction == null)
            {
                _transaction = this._connection.BeginTransaction();
            }
            else if (_transaction.Connection.State != ConnectionState.Open)
            {
                _transaction = this._connection.BeginTransaction();
            }
            return _transaction;
        }

        public void Close(IDbConnection dbConnection)
        {
            dbConnection.Close();
        }
        public void Commit()
        {
            try
            {
                _transaction.Commit();
            }
            catch
            {
                _transaction.Rollback();
                throw;
            }
            finally
            {
                _transaction.Dispose();
                //_transaction = _connection.BeginTransaction();
                resetRepositories();
            }
        }

        public void Dispose()
        {
            dispose(true);
            GC.SuppressFinalize(this);
        }

        private void dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    if (_transaction != null)
                    {
                        _transaction.Dispose();
                        _transaction = null;
                    }
                    if (_connection != null)
                    {
                        _connection.Dispose();
                        _connection = null;
                    }
                }
                _disposed = true;
            }
        }

        ~UnitOfWork()
        {
            dispose(false);
        }

        #endregion UnitOfWork base actions 

        #region Repository Implementation

        private void resetRepositories()
        {
            _productRepository = null;
        }

        private IProductRepository _productRepository;
        private CategoriesRepository _categoriesRepository;
        private AttributeRepository _attributeRepository;
        private UserRepository _userRepository;
        private AdminRepository _adminRepository;
        private TaxRepository _taxRepository;
        private ShippingRepository _shippingRepository;
        public IProductRepository productRepository
        {
            get { return _productRepository ?? (_productRepository = new ProductRepository(_connection)); }
        }
        public ICategoriesRepository categoriesRepository
        {
            get { return _categoriesRepository ?? (_categoriesRepository = new CategoriesRepository(_connection)); }
        }
        public IAttributeRepository attributeRepository
        {
            get { return _attributeRepository ?? (_attributeRepository = new AttributeRepository(_connection)); }
        }
        public IUserRepository userRepository
        {
            get { return _userRepository ?? (_userRepository = new UserRepository(_connection)); }
        }
        public IAdminRepository adminRepository
        {
            get { return _adminRepository ?? (_adminRepository = new AdminRepository(_connection)); }
        }
        public ITaxRepository taxRepository
        {
            get { return _taxRepository ?? (_taxRepository = new TaxRepository(_connection)); }
        }
        public IShippingRepository shippingRepository
        {
            get { return _shippingRepository ?? (_shippingRepository = new ShippingRepository(_connection)); }
        }


        #endregion Repository Implementation

    }
}
