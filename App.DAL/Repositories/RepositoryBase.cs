﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace App.DAL.Repositories
{
    internal abstract class RepositoryBase
    {
        protected static IDbConnection _connection { get; private set; }

        public RepositoryBase(IDbConnection connection)
        {
            _connection = connection;
        }
    }
}
