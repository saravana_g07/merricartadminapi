﻿using App.DAL.FilterModel;
using App.DAL.Model;
using App.DAL.Repositories.Interface;
using Dapper;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace App.DAL.Repositories
{
    internal class AdminRepository : RepositoryBase, IAdminRepository
    {
        protected static ILogger Logger => AppLogger.LoggerFactory.CreateLogger(nameof(AdminRepository));

        public AdminRepository(IDbConnection connection)
            : base(connection)
        {
        }

        #region SystemModules

        private string ModuleBaseQuery()
        {
            string query = "";
            query += $@"SELECT * FROM( ";
            query += $@" SELECT module.id,";
            query += $@"    module.modulename,";
            query += $@"    module.parentmoduleid,";
            query += $@"   (SELECT modulename FROM  {SystemModule.Table}  WHERE  id = module.parentmoduleid) AS parentmodulename,";
            query += $@"   url,";
            query += $@"   icon";
            query += $@"    FROM {SystemModule.Table} AS module";
            query += $@"   WHERE isdeleted = 0) AS result";

            return query;
        }

        public List<ModuleInfo> GetAllModule()
        {
            string query = ModuleBaseQuery();
            try
            {
                return _connection.Query<ModuleInfo>(query).ToList();
            }
            catch (Exception ex)
            {
                //Logger.LogError($@"SqlQuery: {query}");
                throw ex;
            }

        }

        public List<int> GetAccessibleModuleIdByRole(List<int> roleId)
        {
            string roles = string.Join(',', roleId.ToArray());
            string query = RoleBaseQuery() + " where id in (" + roles + ") ";
            try
            {
                var accessibleModules = _connection.Query<RoleInfo>(query).Select(x => x.AccessibleModules.Split(',')).ToList();
                List<int> moduleList = new List<int>();
                foreach (var item in accessibleModules)
                {
                    moduleList.AddRange(item.Select(s => int.Parse(s)).ToList());
                }
                return moduleList.Distinct().ToList();
            }
            catch (Exception ex)
            {
                //Logger.LogError($@"SqlQuery: {query}");
                throw ex;
            }

        }

        public Tuple<List<ModuleInfo>, int> GetModuleList(DataTableFilter filter)
        {
            string query = GetModuleFilterQuery(filter.SearchText, filter.SortingColumnIndex, filter.Start, filter.Length, filter.OrderBy);

            string countquery = $@"select count(1) from {SystemModule.Table} where isdeleted=0 ";
            try
            {
                List<ModuleInfo> moduleList = _connection.Query<ModuleInfo>(query).ToList();
                int totalCount = 0;
                object result = _connection.ExecuteScalar(countquery);
                if (result != null)
                {
                    totalCount = int.Parse(result.ToString());
                }
                return new Tuple<List<ModuleInfo>, int>(moduleList, totalCount);
            }
            catch (Exception ex)
            {
                //Logger.LogError($@"SqlQuery: {query}");
                throw ex;
            }
        }

        private string GetModuleFilterQuery(string searchText, int sortingColumnIndex, int start, int length, string orderBy)
        {
            string query = ModuleBaseQuery();

            if (searchText.Trim().Length > 0)
            {
                query += "    WHERE result.id LIKE '%" + searchText + "%'";
                query += "    OR result.parentmodulename LIKE '%" + searchText + "%'";
                query += "    OR result.url LIKE '%" + searchText + "%'";
            }
            switch (sortingColumnIndex)
            {
                case 1:
                    query += " order by result.id ";
                    break;
                case 2:
                    query += " order by result.modulename ";
                    break;
                case 3:
                    query += " order by result.parentmodulename ";
                    break;
                case 4:
                    query += " order by url ";
                    break;
            }
            if (orderBy == "desc")
            {
                query += " desc ";
            }
            query += " limit " + start + "," + length + " ";

            return query;
        }

        #endregion SystemModules

        #region SystemRoles

        private string RoleBaseQuery()
        {
            string query = "";
            query += $@"SELECT * FROM( ";
            query += $@" SELECT id, rolename, issystemdefined, accessiblemodules, ";
            query += $@" CASE ";
            query += $@"     WHEN accessiblemodules<> '' THEN(select GROUP_CONCAT(DISTINCT module.modulename ORDER BY module.id SEPARATOR', ') ";
            query += $@"           from systemmodules as module where FIND_IN_SET(module.id, roles.accessiblemodules) ) ";
            query += $@"     ELSE 'All Access' ";
            query += $@" END as 'modulename' ";
            query += $@" FROM systemroles as roles where isdeleted = 0 and id > 0) AS result";

            return query;
        }

        public List<RoleInfo> GetAllRoles()
        {
            string query = RoleBaseQuery();
            try
            {
                return _connection.Query<RoleInfo>(query).ToList();
            }
            catch (Exception ex)
            {
                //Logger.LogError($@"SqlQuery: {query}");
                throw ex;
            }

        }

        public Tuple<List<RoleInfo>, int> GetRoleList(DataTableFilter filter)
        {
            string query = GetRoleFilterQuery(filter.SearchText, filter.SortingColumnIndex, filter.Start, filter.Length, filter.OrderBy);

            string countquery = $@"select count(1) from {SystemRole.Table} where isdeleted=0 and id>0";
            try
            {
                List<RoleInfo> roleList = _connection.Query<RoleInfo>(query).ToList();
                int totalCount = 0;
                object result = _connection.ExecuteScalar(countquery);
                if (result != null)
                {
                    totalCount = int.Parse(result.ToString());
                }
                return new Tuple<List<RoleInfo>, int>(roleList, totalCount);
            }
            catch (Exception ex)
            {
                //Logger.LogError($@"SqlQuery: {query}");
                throw ex;
            }
        }

        private string GetRoleFilterQuery(string searchText, int sortingColumnIndex, int start, int length, string orderBy)
        {
            string query = RoleBaseQuery();

            if (searchText.Trim().Length > 0)
            {
                query += "    WHERE result.id LIKE '%" + searchText + "%'";
                query += "    OR result.rolename LIKE '%" + searchText + "%'";
                query += "    OR result.modulename LIKE '%" + searchText + "%'";
            }
            switch (sortingColumnIndex)
            {
                case 1:
                    query += " order by result.id ";
                    break;
                case 2:
                    query += " order by result.rolename ";
                    break;
                case 3:
                    query += " order by result.modulename ";
                    break;
            }
            if (orderBy == "desc")
            {
                query += " desc ";
            }
            query += " limit " + start + "," + length + " ";

            return query;
        }
        
        /// <summary>
        /// Creates (adds) a record to SystemRoles.
        /// </summary>
        /// <param name="SystemRole">SystemRole value(s) to create.</param>
        /// <returns>SystemRole.Id of newly created record.</returns>
        public int CreateRole(SystemRole role)
        {
            string query = $@"INSERT INTO {SystemRole.Table}
                    (rolename, accessibleModules, createdOn,  lastModifiedReason)
                    VALUES 
                    (@RoleName,@AccessibleModules, @CreatedOn, @LastModifiedReason);
                    select last_insert_id();";
            int roleId = 0;
            try
            {
                object result = _connection.ExecuteScalar(query, role);
                if (result != null)
                {
                    roleId = (int)((ulong)result);
                }
                return roleId;
            }
            catch (Exception ex)
            {
                Logger.LogError($@"SqlQuery: {query} using: {JsonConvert.SerializeObject(role)} exception:{ex.Message} ");
                return roleId;
            }
        }
        public bool DeleteRole(SystemRole role)
        {
            string query = $@"UPDATE {SystemRole.Table}
                    SET isDeleted=@IsDeleted, lastmodifiedOn=@LastModifiedOn, lastModifiedReason=@LastModifiedReason
                    WHERE Id=@id;";
            try
            {
                _connection.Execute(query, role);

                return true;
            }
            catch (Exception ex)
            {
                Logger.LogError($@"SqlQuery: {query} using: {JsonConvert.SerializeObject(role)} exception:{ex.Message} ");
                return false;
            }
        }
        public bool UpdateRole(SystemRole role)
        {
            string query = $@"UPDATE {SystemRole.Table}
                    SET rolename=@RoleName, accessibleModules=@AccessibleModules,lastmodifiedOn=@LastModifiedOn, lastModifiedReason=@LastModifiedReason
                    WHERE Id=@id;";
            try
            {
                _connection.Execute(query, role);

                return true;
            }
            catch (Exception ex)
            {
                Logger.LogError($@"SqlQuery: {query} using: {JsonConvert.SerializeObject(role)} exception:{ex.Message} ");
                return false;
            }
        }


        #endregion SystemRoles
    }
}
