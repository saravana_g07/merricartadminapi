﻿using App.DAL.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace App.DAL.Repositories.Interface
{
    public interface ICategoriesRepository
    {
        //Task<int> CreateAsync(Categories categories);
        int InsertCategory(Categories categories);

        IEnumerable<Categories> GetAllCategories();
        bool DeleteCategory(Categories categories);
        bool UpdateCategory(Categories categories);
        bool SetActiveCategories(List<int> nonActiveCategoryIdList);
    }
}
