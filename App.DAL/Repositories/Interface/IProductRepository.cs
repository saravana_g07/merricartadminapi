﻿using System;
using System.Collections.Generic;
using System.Text;
using App.DAL.Model;
using App.DAL.FilterModel;

namespace App.DAL.Repositories.Interface
{
    public interface IProductRepository
    {
        Tuple<IEnumerable<IProductList>, int> GetAllProducts(DataTableFilter filter);
        IEnumerable<Products> GetProductByID(string id);
        IEnumerable<Categories> GetAllCategories();
        IEnumerable<Attributes> GetAttributes();
        int CreateNewProduct(Products products);
        bool DeleteProduct(Products productInfo);
        bool UpdateProduct(Products productInfo);
        bool InsertProductImage(List<string> productImagesNameList, int productId);
        bool DeleteProductImagesById(List<string> productImageIdList);
        List<string> GetProductImageNameByProdyctId(int productId);
    }
}
