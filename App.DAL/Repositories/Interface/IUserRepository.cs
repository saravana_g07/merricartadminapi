﻿using App.DAL.FilterModel;
using App.DAL.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace App.DAL.Repositories.Interface
{
    public interface IUserRepository
    {
        int InsertUser(User user);
        bool UpdateUser(User user);
        bool DeleteUser(User user);
        bool UpdateUserPassword(User user);
        Tuple<IEnumerable<IUserInfo>, int> GetUserList(DataTableFilter filter);
        IUserInfo GetUserByEmailId(string loginEmail);
        IEnumerable<PasswordInfo> GetUserPassword(string loginEmail);
        string GetUserNameByEmailId(string loginEmail);
        bool UpdateUserImage(User user);
        bool UpdateMyProfile(User user);
    }
}

