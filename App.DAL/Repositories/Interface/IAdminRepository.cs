﻿using App.DAL.FilterModel;
using App.DAL.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace App.DAL.Repositories.Interface
{
    public interface IAdminRepository
    {
        List<ModuleInfo> GetAllModule();
        Tuple<List<ModuleInfo>, int> GetModuleList(DataTableFilter filter);
        List<RoleInfo> GetAllRoles();
        Tuple<List<RoleInfo>, int> GetRoleList(DataTableFilter filter);
        List<int> GetAccessibleModuleIdByRole(List<int> roleId);
        int CreateRole(SystemRole role);
        bool DeleteRole(SystemRole role);
        bool UpdateRole(SystemRole role);
    }
}
