﻿using App.DAL.FilterModel;
using App.DAL.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace App.DAL.Repositories.Interface
{
    public interface IAttributeRepository
    {
        Tuple<IEnumerable<Attributes>, int> GetAllAttributes(DataTableFilter filter);
        IEnumerable<Attributes> GetAllActiveAttributes();
        int InsertAttribute(Attributes attribute);
        bool UpdateAttribute(Attributes attribute);
        bool DeleteAttribute(Attributes attribute);
    }
}
