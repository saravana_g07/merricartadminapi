﻿using App.DAL.Model;
using App.DAL.Repositories.Interface;
using Dapper;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace App.DAL.Repositories
{
    internal class CategoriesRepository : RepositoryBase, ICategoriesRepository
    {
        protected static ILogger Logger => AppLogger.LoggerFactory.CreateLogger(nameof(CategoriesRepository));

        public CategoriesRepository(IDbConnection connection)
            : base(connection)
        {
        }

        /// <summary>
        /// Returns the SQL Query selecting all data to hydrate the Categories class data model.
        /// </summary>
        /// <remarks>
        /// This may be a complex SQL query with JOINs as needed.  But the query must not
        /// contain ORDER BY, OFFSET, or TOP declaritives.
        /// </remarks>
        public static string BaseQuery()
        {
            return $@"SELECT * FROM {Categories.Table} WHERE isDeleted=0";
        }


        #region CRUD Operations

        /// <summary>
        /// Creates (adds) a record to Categoriess.
        /// </summary>
        /// <param name="Categories">Categories value(s) to create.</param>
        /// <returns>Categories.Id of newly created record.</returns>
        //public async Task<int> CreateAsync(Categories categories)
        //{
        //    string query = $@"INSERT INTO {Categories.Table}
        //            (name, parentId, isHomePageCategory, createdOn, createdBy, lastmodifedBy, lastModifiedReason)
        //            VALUES 
        //            (@Name, @ParentId, @IsHomePageCategory, @CreatedOn, @CreatedBy, @LastModifiedBy, @LastModifiedReason);
        //            select last_insert_id();";
        //    try
        //    {
        //        int categoryId= await _connection.QuerySingleAsync<int>(query, categories);
        //        return categoryId;
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.LogError($@"SqlQuery: {query} using: {JsonConvert.SerializeObject(categories)} exception:{ex.Message} ");
        //        return 0;
        //    }
        //}

        /// <summary>
        /// Creates (adds) a record to Categoriess.
        /// </summary>
        /// <param name="Categories">Categories value(s) to create.</param>
        /// <returns>Categories.Id of newly created record.</returns>
        public int InsertCategory(Categories categories)
        {
            string query = $@"INSERT INTO {Categories.Table}
                    (name, parentId, isActive, createdOn, attributeIds, categoryimage, lastModifiedReason)
                    VALUES 
                    (@Name, @ParentId, @IsActive, @CreatedOn,@AttributeIds,@CategoryImage, @LastModifiedReason);
                    select last_insert_id();";
            int categoryId = 0;
            try
            {
                object result = _connection.ExecuteScalar(query, categories);
                if (result != null)
                {
                    categoryId = (int)((ulong)result);
                }
                return categoryId;
            }
            catch (Exception ex)
            {
                Logger.LogError($@"SqlQuery: {query} using: {JsonConvert.SerializeObject(categories)} exception:{ex.Message} ");
                return categoryId;
            }
        }
        public bool DeleteCategory(Categories categories)
        {
            string query = $@"UPDATE {Categories.Table}
                    SET isDeleted=@IsDeleted, lastmodifiedOn=@LastModifiedOn, lastModifiedReason=@LastModifiedReason
                    WHERE Id=@id;";
            try
            {
                _connection.Execute(query, categories);

                return true;
            }
            catch (Exception ex)
            {
                Logger.LogError($@"SqlQuery: {query} using: {JsonConvert.SerializeObject(categories)} exception:{ex.Message} ");
                return false;
            }
        }
        public bool UpdateCategory(Categories categories)
        {
            string query = $@"UPDATE {Categories.Table}
                    SET name=@Name, isActive=@IsActive,attributeIds=@AttributeIds,categoryimage=@CategoryImage,
                        lastmodifiedOn=@LastModifiedOn, lastModifiedReason=@LastModifiedReason
                    WHERE Id=@id;";
            try
            {
                _connection.Execute(query, categories);

                return true;
            }
            catch (Exception ex)
            {
                Logger.LogError($@"SqlQuery: {query} using: {JsonConvert.SerializeObject(categories)} exception:{ex.Message} ");
                return false;
            }
        }

        public bool SetActiveCategories(List<int> nonActiveCategoryIdList)
        {

            if (nonActiveCategoryIdList.Count == 0)
            {
                nonActiveCategoryIdList.Add(-1);
            }

            string setNonActiveQuery = "UPDATE categories set isActive=0 where id in (" + string.Join(',', nonActiveCategoryIdList.ToArray()) + ") and isDeleted=0";
            string setActiveQuery = "UPDATE categories set isActive=1 where id not in (" + string.Join(',', nonActiveCategoryIdList.ToArray()) + ") and isDeleted=0";
            try
            {
                _connection.Execute(setNonActiveQuery);
                _connection.Execute(setActiveQuery);

                return true;
            }
            catch (Exception ex)
            {
                Logger.LogError($@"exception occured while update ActiveCategories. exception:{ex.Message} ");
                return false;
            }
        }



        /// <summary>
        /// Reads a list of Categories records.
        /// </summary>
        /// <returns>IEnumerable of all Categories records.</returns>
        public IEnumerable<Categories> GetAllCategories()
        {
            string query = BaseQuery();
            try
            {
                return _connection.Query<Categories>(query);
            }
            catch (Exception ex)
            {
                Logger.LogError($@"SqlQuery: {query}");
                throw ex;
            }
        }

        #endregion
    }
}
