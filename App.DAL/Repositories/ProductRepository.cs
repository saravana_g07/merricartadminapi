﻿using App.DAL.Model;
using App.DAL.FilterModel;
using App.DAL.Repositories.Interface;
using Dapper;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.IO;
using System.Web;

namespace App.DAL.Repositories
{

    internal class ProductRepository : RepositoryBase, IProductRepository
    {
        protected static ILogger Logger => AppLogger.LoggerFactory.CreateLogger(nameof(ProductRepository));

        public ProductRepository(IDbConnection connection)
            : base(connection)
        {
        }


        public Tuple<IEnumerable<IProductList>, int> GetAllProducts(DataTableFilter filter)
        {
            string query = GetProductFilterQuery(filter.SearchText, filter.SortingColumnIndex, filter.Start, filter.Length, filter.OrderBy);
            string countquery = $@"select count(1) from {Products.Table} where isdeleted=0 ";
            try
            {
                IEnumerable<IProductList> prodList = _connection.Query<ProductList>(query);
                int totalCount = 0;
                object result = _connection.ExecuteScalar(countquery);
                if (result != null)
                {
                    totalCount = int.Parse(result.ToString());
                }
                return new Tuple<IEnumerable<IProductList>, int>(prodList, totalCount);
            }
            catch (Exception ex)
            {
                Logger.LogError($@"SqlQuery: {query}");
                throw ex;
            }
        }

        public IEnumerable<Products> GetProductByID(string id)
        {
            string query = $@"select * from {Products.Table} where id=" + id;
            try
            {
                return _connection.Query<Products>(query);
            }
            catch (Exception ex)
            {
                Logger.LogError($@"SqlQuery: {query}");
                throw ex;
            }
        }

        private string GetProductFilterQuery(string searchText, int sortingColumnIndex, int start, int length, string orderBy)
        {
            string query = $@" SELECT * FROM (SELECT P.id, P.name,( select pimg.imagename from productimages pimg where pimg.isdeleted=0 and pimg.productid=P.id limit 1) productimage, (select GROUP_CONCAT(DISTINCT c1.name ORDER BY c1.id SEPARATOR', ') from categories c1 where FIND_IN_SET(c1.id , P.Category)  ) category, P.actualprice, P.specialprice, P.sku, P.quantity,P.datefrom,P.dateto FROM {Products.Table} as P ";
            query += " where P.isdeleted=0) prodTable ";
            if (searchText.Trim().Length > 0)
            {
                query += " where ( prodTable.id like '%" + searchText + "%'  or prodTable.name like '%" + searchText + "%'  or prodTable.category like '%" + searchText + "%'  or prodTable.actualprice like '%" + searchText + "%' ";
                query += " or prodTable.specialprice like '%" + searchText + "%' or prodTable.sku like '%" + searchText + "%'";
                query += " or prodTable.quantity like '%" + searchText + "%' or prodTable.datefrom like '%" + searchText + "%' or prodTable.dateto like '%" + searchText + "%' )";
            }
            switch (sortingColumnIndex)
            {
                case 0:
                    query += " order by id ";
                    break;
                case 1:
                    query += " order by name ";
                    break;
                case 5:
                    query += " order by sku ";
                    break;
                case 6:
                    query += " order by quantity ";
                    break;
                case 7:
                    query += " order by datefrom ";
                    break;
                case 8:
                    query += " order by dateto ";
                    break;
            }
            if (orderBy == "desc")
            {
                query += " desc ";
            }
            query += " limit " + start + "," + length + " ";

            return query;
        }



        public IEnumerable<Categories> GetAllCategories()
        {
            string query = $@"SELECT * FROM {Categories.Table} WHERE isDeleted=0 and isActive=1";
            try
            {
                return _connection.Query<Categories>(query);
            }
            catch (Exception ex)
            {
                Logger.LogError($@"SqlQuery: {query}");
                throw ex;
            }
        }

        public IEnumerable<Attributes> GetAttributes()
        {
            string query = $@"SELECT id,name,type FROM {Attributes.Table} WHERE isDeleted=0 and isactive=1";
            try
            {
                return _connection.Query<Attributes>(query);
            }
            catch (Exception ex)
            {
                Logger.LogError($@"SqlQuery: {query}");
                throw ex;
            }
        }

        public int CreateNewProduct(Products products)
        {
            string query = $@"INSERT INTO {Products.Table}
                    (name, category, actualprice,specialprice, sku, quantity, datefrom, dateto, categoryattributes, isDeleted, createdOn, lastModifiedReason)
                    VALUES 
                    (@Name, @Category, @ActualPrice,@SpecialPrice, @Sku, @Quantity, @DateFrom, @DateTo, @CategoryAttributes, @IsDeleted, @CreatedOn, @LastModifiedReason);
                    select last_insert_id();";
            int productId = 0;
            try
            {
                object result = _connection.ExecuteScalar(query, products);
                if (result != null)
                {
                    productId = (int)((ulong)result);
                }
                return productId;
            }
            catch (Exception ex)
            {
                Logger.LogError($@"SqlQuery: {query} using: {JsonConvert.SerializeObject(products)} exception:{ex.Message} ");
                return productId;
            }
        }
        public bool DeleteProduct(Products products)
        {
            string query = $@"UPDATE {Products.Table}
                    SET isDeleted=@IsDeleted WHERE Id=@id;";
            try
            {
                _connection.Execute(query, products);

                return true;
            }
            catch (Exception ex)
            {
                Logger.LogError($@"SqlQuery: {query} using: {JsonConvert.SerializeObject(products)} exception:{ex.Message} ");
                return false;
            }
        }
        public bool UpdateProduct(Products products)
        {
            string query = $@"UPDATE {Products.Table}
                    SET name=@Name, category=@Category, actualprice=@ActualPrice, specialprice=@SpecialPrice, 
                    sku=@Sku, quantity=@Quantity, datefrom=@DateFrom,dateto=@DateTo, categoryattributes=@CategoryAttributes,
                    lastModifiedReason=@LastModifiedOn ,lastModifiedReason=@LastModifiedReason
                    WHERE Id=@id;";
            try
            {
                _connection.Execute(query, products);

                return true;
            }
            catch (Exception ex)
            {
                Logger.LogError($@"SqlQuery: {query} using: {JsonConvert.SerializeObject(products)} exception:{ex.Message} ");
                return false;
            }
        }

        public bool InsertProductImage(List<string> productImagesNameList, int productId)
        {
            string query = string.Empty;
            try
            {
                query = $@"INSERT INTO {ProductImages.Table}
                    (productid, imagename)
                    VALUES ";
                bool isFirst = true;
                foreach (string productImageName in productImagesNameList)
                {
                    if (!isFirst)
                    {
                        query += ",";
                    }
                    query += " (" + productId + ", '" + productImageName + "')";
                    isFirst = false;
                }

                _connection.Execute(query);

                return true;
            }
            catch (Exception ex)
            {
                Logger.LogError($@"SqlQuery: {query} exception:{ex.Message} ");
                return false;
            }
        }

        public bool DeleteProductImagesById(List<string> productImageIdList)
        {
            productImageIdList = productImageIdList.Select(x => x = "'" + x + "'").ToList();
            string productImageIds = productImageIdList?.Count > 0 ? string.Join(',', productImageIdList) : "''";
            string query = $@"UPDATE {ProductImages.Table}
                    SET isdeleted=1
                    WHERE imagename in (" + productImageIds + ");";
            try
            {
                _connection.Execute(query, new { productImageIds });

                return true;
            }
            catch (Exception ex)
            {
                Logger.LogError($@"SqlQuery: {query} using: {JsonConvert.SerializeObject(productImageIds)} exception:{ex.Message} ");
                return false;
            }
        }

        public List<string> GetProductImageNameByProdyctId(int productId)
        {
            string query = $@"SELECT imagename  FROM {ProductImages.Table} WHERE isDeleted=0 and productId=@productId";
            try
            {
                return _connection.Query<string>(query, new { productId }).ToList();
            }
            catch (Exception ex)
            {
                Logger.LogError($@"SqlQuery: {query}");
                throw ex;
            }

        }
    }

}
