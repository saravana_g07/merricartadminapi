﻿using App.DAL.Repositories.Interface;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace App.DAL.Repositories
{
    internal class ShippingRepository : RepositoryBase, IShippingRepository
    {
        protected static ILogger Logger => AppLogger.LoggerFactory.CreateLogger(nameof(ShippingRepository));

        public ShippingRepository(IDbConnection connection)
            : base(connection)
        {
        }
    }
}
