﻿using App.DAL.FilterModel;
using App.DAL.Model;
using App.DAL.Repositories.Interface;
using Dapper;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace App.DAL.Repositories
{
    internal class AttributeRepository : RepositoryBase, IAttributeRepository
    {
        protected static ILogger Logger => AppLogger.LoggerFactory.CreateLogger(nameof(CategoriesRepository));

        public AttributeRepository(IDbConnection connection)
            : base(connection)
        {
        }
        /// <summary>
        /// Returns the SQL Query selecting all data to hydrate the attribute class data model.
        /// </summary>
        /// <remarks>
        /// This may be a complex SQL query with JOINs as needed.  But the query must not
        /// contain ORDER BY, OFFSET, or TOP declaritives.
        /// </remarks>
        public static string BaseQuery()
        {
            return $@"SELECT * FROM {Attributes.Table} WHERE isDeleted=0";
        }


        /// <summary>
        /// Reads a list of Categories records.
        /// </summary>
        /// <returns>IEnumerable of all Categories records.</returns>
        public Tuple<IEnumerable<Attributes>, int> GetAllAttributes(DataTableFilter filter)
        {
            string query = GetAttributeFilterQuery(filter.SearchText, filter.SortingColumnIndex, filter.Start, filter.Length, filter.OrderBy);
            string countquery = $@"select count(1) from {Attributes.Table} where isdeleted=0 ";
            try
            {
                IEnumerable<Attributes> attributeList = _connection.Query<Attributes>(query);
                int totalCount = 0;
                object result = _connection.ExecuteScalar(countquery);
                if (result != null)
                {
                    totalCount = int.Parse(result.ToString());
                }
                return new Tuple<IEnumerable<Attributes>, int>(attributeList, totalCount);
            }
            catch (Exception ex)
            {
                Logger.LogError($@"SqlQuery: {query}");
                throw ex;
            }
        }
        public IEnumerable<Attributes> GetAllActiveAttributes()
        {

            string query = BaseQuery() + " and isactive=1 ";
            try
            {
                IEnumerable<Attributes> attributeList = _connection.Query<Attributes>(query);
                return attributeList;
            }
            catch (Exception ex)
            {
                Logger.LogError($@"SqlQuery: {query}");
                throw ex;
            }
        }

        private string GetAttributeFilterQuery(string searchText, int sortingColumnIndex, int start, int length, string orderBy)
        {
            string query = $@"SELECT id, name, isactive FROM {Attributes.Table} where isdeleted=0";
            if (searchText.Trim().Length > 0)
            {
                query += " and ( id like '%" + searchText + "%' or name like '%" + searchText + "%' )";
            }
            switch (sortingColumnIndex)
            {
                case 1:
                    query += " order by name ";
                    break;
            }
            if (orderBy == "desc")
            {
                query += " desc ";
            }
            query += " limit " + start + "," + length + " ";

            return query;
        }


        public int InsertAttribute(Attributes attribute)
        {

            string query = $@"INSERT INTO {Attributes.Table} 
                     (name,isactive,type,createdOn,lastModifiedReason)
                     VALUES
                     (@Name,@IsActive,@Type,@CreatedOn, @LastModifiedReason);
                    select last_insert_id(); ";

            int attributeId = 0;
            try
            {
                object result = _connection.ExecuteScalar(query, attribute);
                if (result != null)
                {
                    attributeId = (int)((ulong)result);
                }
                return attributeId;
            }
            catch (Exception ex)
            {
                Logger.LogError($@"SqlQuery: {query} using: {JsonConvert.SerializeObject(attribute)} exception:{ex.Message} ");
                return attributeId;
            }
        }

        public bool UpdateAttribute(Attributes attribute)
        {
            string query = $@"UPDATE {Attributes.Table}
                            SET
                            name = @Name,isactive = @IsActive,
                            lastmodifiedOn=@LastModifiedOn,lastModifiedReason = @LastModifiedReason
                            WHERE id =@Id;";
            try
            {
                _connection.Execute(query, attribute);

                return true;
            }
            catch (Exception ex)
            {
                Logger.LogError($@"SqlQuery: {query} using: {JsonConvert.SerializeObject(attribute)} exception:{ex.Message} ");
                return false;
            }
        }

        public bool DeleteAttribute(Attributes attribute)
        {
            string query = $@"UPDATE {Attributes.Table}
                    SET isDeleted=@IsDeleted, lastmodifiedOn=@LastModifiedOn, lastModifiedReason=@LastModifiedReason
                    WHERE Id=@id;";
            try
            {
                _connection.Execute(query, attribute);

                return true;
            }
            catch (Exception ex)
            {
                Logger.LogError($@"SqlQuery: {query} using: {JsonConvert.SerializeObject(attribute)} exception:{ex.Message} ");
                return false;
            }
        }

    }
}
