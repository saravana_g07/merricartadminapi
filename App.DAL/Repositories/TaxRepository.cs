﻿using App.DAL.Model;
using App.DAL.Repositories.Interface;
using Dapper;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace App.DAL.Repositories
{
    internal class TaxRepository : RepositoryBase, ITaxRepository
    {
        protected static ILogger Logger => AppLogger.LoggerFactory.CreateLogger(nameof(TaxRepository));

        public TaxRepository(IDbConnection connection)
            : base(connection)
        {
        }

        public List<Country> GetAllCountry()
        {
            string query = $@"SELECT * FROM {Country.Table} Where isdeleted=0 ";
            try
            {
                return _connection.Query<Country>(query).ToList();
            }
            catch (Exception ex)
            {
                //Logger.LogError($@"SqlQuery: {query}");
                throw ex;
            }
        }

        public int InsertCountry(Country country)
        {
            string query = $@"INSERT INTO {Country.Table} (countryname,createdOn,lastModifiedReason)
                     VALUES (@CountryName,@CreatedOn,@LastModifiedReason);
                    select last_insert_id(); ";

            int userid = 0;
            try
            {
                object result = _connection.ExecuteScalar(query, country);
                if (result != null)
                {
                    userid = (int)((ulong)result);
                }
                return userid; 
            }
            catch (Exception ex)
            {
                //Logger.LogError($@"SqlQuery: {query} using: {JsonConvert.SerializeObject(user)} exception:{ex.Message} ");
                return userid;
            }
        }
    }
}
