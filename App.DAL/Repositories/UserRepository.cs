﻿using App.DAL.FilterModel;
using App.DAL.Model;
using App.DAL.Repositories.Interface;
using Dapper;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace App.DAL.Repositories
{
    internal class UserRepository : RepositoryBase, IUserRepository
    {
        protected static ILogger Logger => AppLogger.LoggerFactory.CreateLogger(nameof(UserRepository));

        public UserRepository(IDbConnection connection)
            : base(connection)
        {
        }

        public static string BaseQuery()
        {
            string query = "";

            query += $@" SELECT * FROM(  ";
            query += $@"              SELECT  id, firstname, lastname, loginemail, userrole, isactive, recentlogin, userimage ,  ";
            query += $@"                  (select GROUP_CONCAT(DISTINCT roles.rolename ORDER BY roles.id SEPARATOR', ')  ";
            query += $@"                        from {SystemRole.Table} as roles where  roles.isdeleted=0 and FIND_IN_SET(roles.id, users.userrole)  )  ";
            query += $@"                   ";
            query += $@"               as 'rolename' ";
            query += $@"              FROM {User.Table} where users.isdeleted = 0 ) AS result ";

            return query;
        }
        public int InsertUser(User user)
        {

            string query = $@"INSERT INTO {User.Table} 
                     (firstname,lastname,loginemail,password,salt,userrole,isactive,recentlogin,createdOn,lastModifiedReason)
                     VALUES
                     (@FirstName,@LastName,@LoginEmail,@Password,@Salt,@UserRole,@IsActive,@RecentLogin,@CreatedOn, @LastModifiedReason);
                    select last_insert_id(); ";

            int userid = 0;
            try
            {
                object result = _connection.ExecuteScalar(query, user);
                if (result != null)
                {
                    userid = (int)((ulong)result);
                }
                return userid;
            }
            catch (Exception ex)
            {
                //Logger.LogError($@"SqlQuery: {query} using: {JsonConvert.SerializeObject(user)} exception:{ex.Message} ");
                return userid;
            }
        }

        public bool UpdateUser(User user)
        {
            string query = $@"UPDATE {User.Table}
                            SET
                            firstname = @FirstName,lastname = @LastName,loginemail = @LoginEmail,userRole = @UserRole,isactive = @IsActive,
                            lastmodifiedOn=@LastModifiedOn,lastModifiedReason = @LastModifiedReason
                            WHERE id =@Id;";
            try
            {
                _connection.Execute(query, user);

                return true;
            }
            catch (Exception ex)
            {
                //Logger.LogError($@"SqlQuery: {query} using: {JsonConvert.SerializeObject(user)} exception:{ex.Message} ");
                return false;
            }
        }

        public bool UpdateMyProfile(User user)
        {
            string query = $@"UPDATE {User.Table}
                            SET
                            firstname = @FirstName,lastname = @LastName,
                            lastmodifiedOn=@LastModifiedOn,lastModifiedReason = @LastModifiedReason
                            WHERE id =@Id;";
            try
            {
                _connection.Execute(query, user);

                return true;
            }
            catch (Exception ex)
            {
                //Logger.LogError($@"SqlQuery: {query} using: {JsonConvert.SerializeObject(user)} exception:{ex.Message} ");
                return false;
            }
        }

        public bool DeleteUser(User user)
        {
            string query = $@"UPDATE {User.Table}
                    SET isDeleted=@IsDeleted, lastmodifiedOn=@LastModifiedOn, lastModifiedReason=@LastModifiedReason
                    WHERE Id=@id;";
            try
            {
                _connection.Execute(query, user);

                return true;
            }
            catch (Exception ex)
            {
                //Logger.LogError($@"SqlQuery: {query} using: {JsonConvert.SerializeObject(user)} exception:{ex.Message} ");
                return false;
            }
        }

        public bool UpdateUserPassword(User user)
        {
            string query = $@"UPDATE {User.Table}
                            SET
                            password = @Password,salt = @Salt,lastmodifiedOn=@LastModifiedOn,lastModifiedReason = @LastModifiedReason
                            where loginEmail=@loginEmail;";
            try
            {
                _connection.Execute(query, user);

                return true;
            }
            catch (Exception ex)
            {
                //Logger.LogError($@"SqlQuery: {query} using: {JsonConvert.SerializeObject(user)} exception:{ex.Message} ");
                return false;
            }
        }

        public Tuple<IEnumerable<IUserInfo>, int> GetUserList(DataTableFilter filter)
        {
            //string query = $@"SELECT id, firstname, lastname, loginemail, isadmin, isactive, recentlogin FROM {User.Table} where isdeleted=0";
            string query = GetUserFilterQuery(filter.SearchText, filter.SortingColumnIndex, filter.Start, filter.Length, filter.OrderBy);
            string countquery = $@"select count(1) from {User.Table} where isdeleted=0 ";
            try
            {
                IEnumerable<IUserInfo> userList = _connection.Query<UserInfo>(query);
                int totalCount = 0;
                object result = _connection.ExecuteScalar(countquery);
                if (result != null)
                {
                    totalCount = int.Parse(result.ToString());
                }
                return new Tuple<IEnumerable<IUserInfo>, int>(userList, totalCount);
            }
            catch (Exception ex)
            {
                //Logger.LogError($@"SqlQuery: {query}");
                throw ex;
            }
        }

        public IUserInfo GetUserByEmailId(string loginEmail)
        {
            string query = BaseQuery() + " where  loginEmail=@LoginEmail;";
            try
            {
                UserInfo result = _connection.Query<UserInfo>(query, new { LoginEmail = loginEmail }).FirstOrDefault();
                result.UserImage = result.UserImage == null ? "no-user.png" : result.UserImage;
                return result;
            }
            catch (Exception ex)
            {
                //Logger.LogError($@"SqlQuery: {query}");
                return null;
            }
        }

        private string GetUserFilterQuery(string searchText, int sortingColumnIndex, int start, int length, string orderBy)
        {
            string query = BaseQuery() + " where result.userrole <> '-1'";

            if (searchText.Trim().Length > 0)
            {
                query += "    AND( result.id LIKE '%" + searchText + "%'";
                query += "    OR result.firstname LIKE '%" + searchText + "%'";
                query += "    OR result.lastname LIKE '%" + searchText + "%'";
                query += "    OR result.loginemail LIKE '%" + searchText + "%'";
                query += "    OR result.rolename LIKE '%" + searchText + "%' )";
            }
            switch (sortingColumnIndex)
            {
                case 2:
                    query += " order by result.firstname ";
                    break;
                case 3:
                    query += " order by result.lastname ";
                    break;
                case 4:
                    query += " order by result.loginemail ";
                    break;
                case 5:
                    query += " order by result.rolename ";
                    break;
                case 6:
                    query += " order by result.isactive ";
                    break;
            }
            if (orderBy == "desc")
            {
                query += " desc ";
            }
            query += " limit " + start + "," + length + " ";

            return query;
        }

        public IEnumerable<PasswordInfo> GetUserPassword(string loginEmail)
        {
            string query = $@"SELECT password, salt FROM {User.Table} WHERE loginEmail=@LoginEmail;";
            try
            {
                return _connection.Query<PasswordInfo>(query, new { LoginEmail = loginEmail });
            }
            catch (Exception ex)
            {
                //Logger.LogError($@"SqlQuery: {query}");
                throw ex;
            }
        }


        public string GetUserNameByEmailId(string loginEmail)
        {
            string query = $@"SELECT firstname,lastname FROM {User.Table} WHERE loginEmail=@LoginEmail;";
            try
            {
                User user = _connection.Query<User>(query, new { LoginEmail = loginEmail }).FirstOrDefault();
                return user.FirstName + " " + user.LastName;
            }
            catch (Exception ex)
            {
                //Logger.LogError($@"SqlQuery: {query}");
                throw ex;
            }
        }

        public bool UpdateUserImage(User user)
        {
            string query = $@"UPDATE {User.Table}
                            SET
                            userImage = @UserImage,
                            lastmodifiedOn=@LastModifiedOn,lastModifiedReason = @LastModifiedReason
                            WHERE id =@Id;";
            try
            {
                _connection.Execute(query, user);

                return true;
            }
            catch (Exception ex)
            {
                //Logger.LogError($@"SqlQuery: {query} using: {JsonConvert.SerializeObject(user)} exception:{ex.Message} ");
                return false;
            }
        }

    }
}
