﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace App.DAL
{
    public class AppLogger
    {
        public static ILoggerFactory LoggerFactory { get; protected set; }
        public AppLogger(ILoggerFactory loggerFactory)
        {
            LoggerFactory = loggerFactory;
        }
    }
}
