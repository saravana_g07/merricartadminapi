﻿using App.DAL.Repositories.Interface;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace App.DAL
{
    public interface IUnitOfWork : IDisposable
    {
        IDbTransaction BeginTransaction();
        void Commit();

        IProductRepository productRepository { get; }
        ICategoriesRepository categoriesRepository { get; }
        IAttributeRepository attributeRepository { get; }
        IUserRepository userRepository { get; }
        IAdminRepository adminRepository { get; }
        ITaxRepository taxRepository { get; }
        IShippingRepository shippingRepository { get; }
    }
}
